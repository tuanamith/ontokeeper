package edu.tmc.uth.ontokeeper.core;

import org.vaadin.grid.cellrenderers.client.editoraware.renderers.CheckboxRenderer;
import org.vaadin.teemu.switchui.Switch;

import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitEvent;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Sizeable;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.MouseEventDetails;
import com.vaadin.shared.ui.grid.renderers.RendererClickRpc;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.Column;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.renderers.ClickableRenderer.RendererClickEvent;
import com.vaadin.ui.renderers.Renderer;

import de.datenhahn.vaadin.componentrenderer.ComponentRenderer;
import de.datenhahn.vaadin.componentrenderer.grid.ComponentGrid;
import de.datenhahn.vaadin.componentrenderer.grid.ComponentGridDecorator;
import edu.tmc.uth.ontokeeper.ProjectProfile;
import edu.tmc.uth.ontokeeper.backend.DataServerService;
import edu.tmc.uth.ontokeeper.backend.data.NL;
import edu.tmc.uth.ontokeeper.data.ontology.Lexicon;
import edu.tmc.uth.ontokeeper.renderers.CustomGridComponenets;
import edu.tmc.uth.ontokeeper.renderers.SwitchRenderer;

public class ExpertPanel extends PanelContent {
	
	private DataServerService dss = null;

	//TODO: Check URL for unique id string (name of table)

	/* (non-Javadoc)
	 * @see edu.tmc.uth.ontokeeper.core.PanelContent#enter(com.vaadin.navigator.ViewChangeListener.ViewChangeEvent)
	 */
	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		System.out.println("enter");
		super.enter(event);
		
		//refresh datasource
		
		//pull data from postgre
		if(dss == null) dss = DataServerService.get();
		long ontokey =((ProjectProfile)VaadinSession.getCurrent().getSession().getAttribute("ProjectProfile")).getOntology_ID();
		dataSource.addAll(dss.getNLData(ontokey));
		
	}

	public static final String VIEW_NAME = "Review";
	private BeanItemContainer<NL> dataSource = null;

	private Grid grid = null;

	public ExpertPanel() {
		// TODO Auto-generated constructor stub

		createTitle(this.VIEW_NAME);
		createSubtitle("Review each sentence provided. Indicate whether the sentence is true or false, and make any corrections necessary");
		//createTabs();
		composeUI();

	}

	private void composeUI() {
		// TODO Auto-generated method stub
		
		//System.out.println("compose ui");
		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		layout.setSpacing(true);

		dataSource = new BeanItemContainer<NL>(NL.class);
		
		GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(dataSource);

		grid = new Grid("Knowledge sentences");
		grid.setContainerDataSource(gpc);
				

		grid.setWidth("100%");
		grid.setHeight("100%");
		grid.setResponsive(true);


		grid.setColumnOrder("id","nl", "is_correct", "notes");
		grid.setColumns("id","nl","is_correct", "notes");
		
		grid.setColumnOrder("id");
		grid.getColumn("id").setHidden(true);
		grid.getColumn("nl").setHeaderCaption("Generated Statements");
		
		grid.getColumn("is_correct").setHeaderCaption("Is Correct?");
		grid.getColumn("is_correct").setWidth(100);
		grid.getColumn("notes").setHeaderCaption("Expert's notes");
		grid.getColumn("notes").setWidth(300);
		
		//grid.getColumn("id").setEditable(false);
		

		grid.setEditorEnabled(true);
		grid.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler(){

			@Override
			public void preCommit(CommitEvent commitEvent) throws CommitException {
				// TODO Auto-generated method stub
			}

			@Override
			public void postCommit(CommitEvent commitEvent) throws CommitException {
				// TODO Auto-generated method stub
				
				
				NL nlData = new NL();
				nlData.setIs_correct(((Boolean)commitEvent.getFieldBinder().getField("is_correct").getValue()).booleanValue());
				nlData.setNl((String) commitEvent.getFieldBinder().getField("nl").getValue());
				nlData.setNotes((String)commitEvent.getFieldBinder().getField("notes").getValue());
				nlData.setId(Long.parseLong((String) commitEvent.getFieldBinder().getField("id").getValue()
						.toString().replaceAll(",", "")));
				
				dss.updateNLData(nlData, "nldata");
				
				//System.out.println("id: " + commitEvent.getFieldBinder().getField("id").getValue());
				Notification.show("Field updated");
				
			}
			
		});
		

		layout.addComponent(grid);
		this.addComponent(layout);


	}

	private void createTabs() {
		// TODO Auto-generated method stub

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
