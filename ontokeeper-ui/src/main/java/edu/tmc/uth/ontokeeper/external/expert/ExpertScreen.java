package edu.tmc.uth.ontokeeper.external.expert;

import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitEvent;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.ui.Button;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

import edu.tmc.uth.ontokeeper.backend.DataServerService;
import edu.tmc.uth.ontokeeper.backend.data.NL;



public class ExpertScreen extends VerticalLayout{
	
	private DataServerService dss = null;
	private BeanItemContainer<NL> dataSource = new BeanItemContainer<NL>(NL.class);
	private Grid grid = null;
	private String tableName = "";
	/**
	 * 
	 */
	private static final long serialVersionUID = -5018112304747486985L;

	public ExpertScreen(String url) {
		// TODO Auto-generated constructor stub
		System.out.println("url parameter: " + url);
		
		
		if(dss == null) {
			dss = DataServerService.get();
		}
		
		tableName = url.toLowerCase();
		
		dataSource.addAll(dss.getExpertNLDataByName(tableName));
		
		this.initUI();
		this.setupUI();
	}
	
	private void initUI(){
		//setSizeFull();
		setMargin(true);
		setSpacing(true);
		//setHeight("100%");
		
	}
	
	private void setupUI(){
		//dataSource = new BeanItemContainer<NL>(NL.class);
		GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(dataSource);
		grid = new Grid("Knowledge sentences");
		grid.setContainerDataSource(gpc);
		grid.setWidth("100%");
		grid.setHeight("100%");
		grid.setResponsive(true);
		grid.setSizeFull();
		
		grid.setColumnOrder("id","nl", "is_correct", "notes");
		grid.setColumns("id","nl","is_correct", "notes");
		
		grid.setColumnOrder("id");
		grid.getColumn("id").setHidden(true);
		grid.getColumn("nl").setHeaderCaption("Generated Statements");
		
		grid.getColumn("is_correct").setHeaderCaption("Is Correct?");
		grid.getColumn("is_correct").setWidth(100);
		grid.getColumn("notes").setHeaderCaption("Expert's notes");
		grid.getColumn("notes").setWidth(300);
		grid.setEditorEnabled(true);
		grid.getEditorFieldGroup().addCommitHandler(new FieldGroup.CommitHandler(){

			@Override
			public void preCommit(CommitEvent commitEvent) throws CommitException {
				// TODO Auto-generated method stub
			}

			@Override
			public void postCommit(CommitEvent commitEvent) throws CommitException {
				// TODO Auto-generated method stub
				
				
				NL nlData = new NL();
				nlData.setIs_correct(((Boolean) commitEvent.getFieldBinder().getField("is_correct").getValue()).booleanValue());
				nlData.setNl((String) commitEvent.getFieldBinder().getField("nl").getValue());
				nlData.setNotes((String)commitEvent.getFieldBinder().getField("notes").getValue());
				nlData.setId(Long.parseLong((String) commitEvent.getFieldBinder().getField("id").getValue()
						.toString().replaceAll(",", "")));
				
				dss.updateNLData(nlData, tableName);
				
				//System.out.println("id: " + commitEvent.getFieldBinder().getField("id").getValue());
				Notification.show("Field updated");
				
			}
			
		});
		
		Label header = new Label("Review each sentence provided.\n"
				+ "Indicate whether the sentence is true or false, and make any corrections necessary under the note sections");
		header.setStyleName("h1");
		addComponent(header);
		addComponent(grid);
		//addComponent(new Label("Indicate whether the sentence is true or false, and make any corrections necessary."));
		
		
	}

	public ExpertScreen(Component... children) {
		super(children);
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	

}
