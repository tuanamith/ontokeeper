package edu.tmc.uth.ontokeeper.samples.about;

import java.io.File;
import java.util.logging.Logger;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.FileResource;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Version;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomLayout;
import com.vaadin.ui.Image;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import edu.tmc.uth.ontokeeper.SEMSService;

public class AboutView extends VerticalLayout implements View {

    public static final String VIEW_NAME = "About";
    
    private final static Logger LOGGER = Logger.getLogger(AboutView.class.getName());
    
    String basepath = VaadinService.getCurrent()
            .getBaseDirectory().getAbsolutePath();

    public AboutView() {
        CustomLayout aboutContent = new CustomLayout("aboutview");
        aboutContent.setStyleName("about-content");
        
        FileResource teamLogo = new FileResource(new File(basepath +
                "/VAADIN/themes/ontokeeper/img/rdf_flyer.png"));

        // you can add Vaadin components in predefined slots in the custom
        // layout
        /*aboutContent.addComponent(
                new Label(FontAwesome.INFO_CIRCLE.getHtml()
                        + " This application is using Vaadin "
                        + Version.getFullVersion(), ContentMode.HTML), "info");*/
        
        aboutContent.addComponent(
                new Label(FontAwesome.INFO_CIRCLE.getHtml()
                        + " OntoKeeper was developed by Ontology Research Group (UTHSC-Houston) "
                        + Version.getFullVersion(), ContentMode.HTML), "info");
        
        aboutContent.addComponent(
        		new Image(null, teamLogo),"logo"
        		);

        setSizeFull();
        setStyleName("about-view");
        addComponent(aboutContent);
        setComponentAlignment(aboutContent, Alignment.MIDDLE_CENTER);
    }

    @Override
    public void enter(ViewChangeEvent event) {
    	
    		
    	
 
    		
    }

}
