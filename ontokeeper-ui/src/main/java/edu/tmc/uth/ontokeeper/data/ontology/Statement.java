package edu.tmc.uth.ontokeeper.data.ontology;

public class Statement{

	private String subject;
	private String verb;
	private String object;

	private String subject_modifier;
	private String verb_modifier;
	private String object_modifier;

	private String type; //axiom type 

	private String  instances;

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public String getInstances() {
		return instances;
	}

	public void setInstances(String instances) {

		this.instances = instances;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getVerb() {
		return verb;
	}

	public void setVerb(String verb) {
		this.verb = verb;
	}

	public String getSubject_modifier() {
		return subject_modifier;
	}

	public void setSubject_modifier(String subject_modifier) {
		this.subject_modifier = subject_modifier;
	}

	public String getVerb_modifier() {
		return verb_modifier;
	}

	public void setVerb_modifier(String verb_modifier) {
		this.verb_modifier = verb_modifier;
	}

	public String getObject_modifier() {
		return object_modifier;
	}

	public void setObject_modifier(String object_modifier) {
		this.object_modifier = object_modifier;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getObject() {
		return object;
	}

	public void setObject(String object) {
		this.object = object;
	}

	/*public void printStatement(){
		if(type.equals("SubClassOf")){

		}
		if(type.equals("ClassAssertion")){

		}
		if(type.equals("ObjectPropertyAssertion")){

		}
		if(type.equals("DifferentIndividuals")){

		}
		if(type.equals("FunctionalDataProperty")){

		}

	}*/

	public String getRawStatement(){
		
		String raw_statement = "";
		
		if(type.equals("SubClassOf")){
			
			raw_statement = this.subject + " is a type of " + this.object;

		}
		if(type.equals("ClassAssertion")){
			raw_statement = this.subject + " is " + this.object;
		}
		if(type.equals("ObjectPropertyAssertion")){
			raw_statement = this.subject + " " + this.verb + " " + this.object;
		}
		if(type.equals("DifferentIndividuals")){
			
			//String instance_string = java.util.Arrays.toString(this.instances);
			//instance_string = instance_string.replaceAll("[", "");
			//instance_string = instance_string.replaceAll("]", "");
			
			raw_statement = this.instances + " are all different from each other";

		}
		if(type.equals("FunctionalDataProperty")){
			
			raw_statement = this.subject + " can have only one value";

		}
		
		return raw_statement;
	}

}
