package edu.tmc.uth.ontokeeper.core;

import java.net.MalformedURLException;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.VerticalLayout;

import edu.tmc.uth.ontokeeper.OntologyService;
import edu.tmc.uth.ontokeeper.ProjectProfile;
import edu.tmc.uth.ontokeeper.WordNetService;
import edu.tmc.uth.ontokeeper.data.ontology.Lexicon;

public class ProcessPanel extends PanelContent {
	
	public static final String VIEW_NAME = "Process";
	private Grid grid = null;
	private BeanItemContainer<Lexicon> dataSource = null;

	public ProcessPanel() {
		// TODO Auto-generated constructor stub
		
		createTitle("Process Project");
		createSubtitle("View the results of the data");
		composeUI();
		
		
	}

	private void composeUI() {
		// TODO Auto-generated method stub
		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		layout.setSpacing(true);
		
		Button processButton = new Button("Process");
		processButton.addClickListener(new Button.ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				// TODO Auto-generated method stub
				
				WordNetService ws = (WordNetService)VaadinSession.getCurrent().getSession().getAttribute("WordNetService");
				try {
					ws.init();
				} catch (MalformedURLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				ws.generateWordSenses();
				
				
				//ws.printProcessedTerms();
				
				//ws.printTerms();
				dataSource.removeAllItems();
				dataSource.addAll(ws.getTerms());
			
				
				Notification.show("Word Senses Generated",
						null,
		                  Notification.Type.TRAY_NOTIFICATION);
				
				

			}
		});
		
		
		//Configure grid
		//dataSource.addContainerProperty(propertyId, type, defaultValue)
		dataSource = new BeanItemContainer<Lexicon>(Lexicon.class);
		
		
		grid = new Grid("Terms/labels from your ontology", dataSource);
        grid.setWidth("100%");
        grid.setHeight("100%");
        grid.setResponsive(true);
        
        grid.setColumnOrder("term", "processed_term", "senses");
        grid.setColumns("term","processed_term","senses");
        
        grid.getColumn("term").setHeaderCaption("Original Term");
        grid.getColumn("processed_term").setHeaderCaption("After Processing");
        grid.getColumn("senses").setHeaderCaption("Number of Senses");
        
        
        grid.setEditorEnabled(true);
       
        //Panel data
        Panel infoPanel = new Panel("Information");
        
        
        
		layout.addComponent(processButton);
		layout.addComponent(grid);
		layout.addComponent(infoPanel);
		this.addComponent(layout);
		
	}
	
	

}
