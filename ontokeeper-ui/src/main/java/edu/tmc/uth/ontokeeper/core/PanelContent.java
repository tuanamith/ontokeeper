package edu.tmc.uth.ontokeeper.core;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

public class PanelContent extends VerticalLayout implements View {
	
	public static final String VIEW_NAME = "";

	public PanelContent() {
		// TODO Auto-generated constructor stub
		this.setMargin(true);
		this.setSpacing(true);
	}
	
	protected void createTitle(String name){
		Label title = new Label(name);
		title.setSizeUndefined();
		title.addStyleName(ValoTheme.LABEL_H1);
		title.addStyleName(ValoTheme.LABEL_NO_MARGIN);
		
		this.addComponent(title);
	}
	
	protected void createSubtitle(String name){
		Label subtitle = new Label(name);
		subtitle.setSizeUndefined();
		subtitle.addStyleName(ValoTheme.LABEL_H2);
		subtitle.addStyleName(ValoTheme.LABEL_NO_MARGIN);
		
		this.addComponent(subtitle);
		
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

}
