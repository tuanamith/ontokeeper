package edu.tmc.uth.ontokeeper;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.vaadin.server.VaadinService;

import edu.mit.jwi.Dictionary;
import edu.mit.jwi.IDictionary;
import edu.mit.jwi.item.IIndexWord;
import edu.mit.jwi.item.POS;
import edu.tmc.uth.ontokeeper.data.ontology.Lexicon;

public class WordNetService {

	String basepath = VaadinService.getCurrent()
			.getBaseDirectory().getAbsolutePath();

	//new File(basepath+"/VAADIN/themes/ontokeeper/ontologies/" + filename);

	private IDictionary dict = null;
	//private static WordNetService instance = null;

	private ArrayList<Lexicon>terms = null;

	private int terms_w_senses = 0;
	private int senses_count = 0;

	/**
	 * @return the terms
	 */
	public ArrayList<Lexicon> getTerms() {
		if(terms == null){
			terms = new ArrayList<Lexicon>();
		}

		return terms;
	}

	/**
	 * @param terms the terms to set
	 */
	public void setTerms(ArrayList<Lexicon> terms) {

		this.terms = terms;
	}

	public WordNetService() {
		// TODO Auto-generated constructor stub
	}

	/*public static WordNetService getInstance(){

		if(instance == null){
			instance = new WordNetService();
		}

		return instance;

	}*/

	public void cleanUpTerms(){
		if(terms == null){
			terms = new ArrayList<Lexicon>();
		}
		else{
			terms.clear();
		}

		//get terms from the OWLService
		/*owlTerms = OntologyService.getInstance().getOWLTerms();

		for(String term: owlTerms){
			String processed_term = term;
			if(processed_term !=null || !processed_term.isEmpty()){

				if(isCamelCased){
					processed_term = buildStringFromArray(StringUtils.splitByCharacterTypeCamelCase(processed_term));
				}

				if(isDelimited){
					String pattern = " a | the | an";
					processed_term = processed_term.replaceAll(pattern, "");
				}

				if(isBracketed){

					processed_term = processed_term.replaceAll("(\\(.+?\\))|(\\{.+?\\})", "");

				}
				if(isUnderscored){

					processed_term = processed_term.replaceAll("_", " ");

				}
				if(isDashed){
					processed_term = processed_term.replaceAll("-", " ");
				}

				owldata.insertProcessedTerm(processed_term);
				//terms.add(new Lexicon().setProcessed_term(processed_term));
				Lexicon item = new Lexicon();
				item.setProcessed_term(processed_term); 
				terms.add(item);

			}
		}*/


	}

	public void init() throws MalformedURLException{
		URL url=null;
		try {
			url = new URL("file", null, basepath+"/VAADIN/wn3/dict");
			dict = new Dictionary(url);
			dict.open();
			if(dict.isOpen()){
				System.out.println("WordNet library is active");
			}


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void generateWordSenses() {
		// TODO Auto-generated method stub
		if(terms == null){
			System.out.println("Terms are empty/null");
			return;
		}

		System.out.println("Count of terms: " + terms.size());

		for(Lexicon term: terms){
			int sense_count = 0;
			String words[] = term.getProcessed_term().trim().split("\\s+");
			for(String word : words){
				//word = word.trim();
				//word = word.toLowerCase();
				if(word !=null && !word.isEmpty()){
					for(POS c : POS.values()){
						IIndexWord idxWord = dict.getIndexWord(word, c);
						if(idxWord != null){
							sense_count = sense_count + idxWord.getWordIDs().size();
						}
					}
				}

			}
			term.setSenses(sense_count);
		}

		this.removeExcessiveSpaces();

	}

	private void removeExcessiveSpaces(){
		for(Lexicon lexicon: terms){
			String term = lexicon.getProcessed_term();
			//System.out.println("Term: " + term);
			term = term.trim().replaceAll(" +", " ");
			//System.out.println("After: " + term);
			lexicon.setProcessed_term(term);
		}
	}

	public int getTotalSenses(){
		senses_count= 0;

		if(terms != null){
			for(Lexicon term: terms){
				senses_count += term.getSenses();
			}
		}


		return senses_count;

	}

	public int getTermsWithSenses(){
		terms_w_senses=0;
		if(terms != null){
			for(Lexicon term: terms){
				if(term.getSenses()>0)terms_w_senses++;
			}
		}


		return terms_w_senses;
	}

	public void printProcessedTerms(){
		for(Lexicon lexicon : terms){
			System.out.println(lexicon.getProcessed_term() + " : " + lexicon.getSenses());
		}
	}

	public int totalTerms(){
		int total = 0;
		
		if(terms !=null){
			total = terms.size();
		}
		
		return total;
	}

	public void printTerms(){
		/*** Prints out nulls ***/
		for(Lexicon lexicon : terms){
			System.out.println(lexicon.getTerm());
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
