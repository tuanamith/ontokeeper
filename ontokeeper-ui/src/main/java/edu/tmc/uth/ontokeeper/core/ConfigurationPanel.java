package edu.tmc.uth.ontokeeper.core;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Accordion;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.StartedEvent;
import com.vaadin.ui.Upload.StartedListener;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import edu.tmc.uth.ontokeeper.OntologyService;
import edu.tmc.uth.ontokeeper.ProjectProfile;
import edu.tmc.uth.ontokeeper.SEMSService;
import edu.tmc.uth.ontokeeper.backend.DataServerService;
import edu.tmc.uth.ontokeeper.backend.NLGService;
import edu.tmc.uth.ontokeeper.backend.data.Ontology;


public class ConfigurationPanel extends PanelContent implements ValueChangeListener{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8355776728343327295L;
	public static final String VIEW_NAME = "Configuration";
	private Accordion organizer = new Accordion();
	private ArrayList<String> allowedMimeTypes = new ArrayList<String>();
	private OntologyUploader receiver = new OntologyUploader();
	String basepath = VaadinService.getCurrent()
			.getBaseDirectory().getAbsolutePath();
	
	CheckBox cbSyntactic = new CheckBox("Syntactic", false);
	CheckBox cbSemantic = new CheckBox("Semantic", false);
	CheckBox cbPragmatic = new CheckBox("Pragmatic", false);
	CheckBox cbSocial = new CheckBox("Social", true);
	
	
	CheckBox cbCamelCase = new CheckBox("Camel Case", true);
	CheckBox cbDeterminers = new CheckBox("Determiters", true);
	CheckBox cbBrackets = new CheckBox("Brackets", true);
	CheckBox cbUnderscores = new CheckBox("Underscores", true);
	CheckBox cbDashes = new CheckBox("Dashes", true);
	//CheckBox cbLabeling = new CheckBox("My ontology uses unique ids", true);
	
	TextField sessionName;
	
	ProjectProfile p =null;
	SEMSService ss = null;
	DataServerService dss = null;
	
	Panel infoPanel = null;
	Button removebutton = new Button("Remove Ontology");
	Label ontologyLabel = new Label();
	
	private long user_id = (long)VaadinSession.getCurrent().getSession().getAttribute("user_id");
	
	private Grid selectOntologyGrid;
	private BeanItemContainer<Ontology> ontologies = null;
	
	public ConfigurationPanel() {
		// TODO Auto-generated constructor stub
		//allowedMimeTypes.add("application/rdf+xml");
		//allowedMimeTypes.add("application/xml");
		
		p = (ProjectProfile)VaadinSession.getCurrent().getSession().getAttribute("ProjectProfile");
		ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
		
		createTitle(this.VIEW_NAME);
		createSubtitle("Provide configuration settings for your ontology file");
		
		
		
		
		addCheckBoxes();
		
		//System.out.println("Class is " + this.getParent().getClass());
	}
	
	private void addInfoPanel(){
		 infoPanel = new Panel("Ontology status");
		 
		 this.addComponent(infoPanel);
		 
		 VerticalLayout contentLayout = new VerticalLayout();
		 contentLayout.addComponent(ontologyLabel);
		 contentLayout.addComponent(removebutton);
		 contentLayout.setSizeUndefined();
		 contentLayout.setMargin(true); 
		 
		 infoPanel.setContent(contentLayout);
		 
		 removebutton.setEnabled(false);
		 ontologyLabel.setCaption("No file uploaded");
		 
	}

	private void addCheckBoxes(){

		organizer.setHeight("100%");
		
		TabSheet fileUses = new TabSheet();
		
		fileUses.setHeight(100.0f, Unit.PERCENTAGE);
		fileUses.addStyleName(ValoTheme.TABSHEET_FRAMED);
		fileUses.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);
		
		
		Upload upload = new Upload("Upload your rdf or owl file here", receiver);
		upload.setButtonCaption("Start Upload");
		upload.addSucceededListener(receiver);
		
		
		sessionName = new TextField();
		sessionName.setInputPrompt("Enter a session name");
		sessionName.setStyleName("sessionText");
		sessionName.setWidth("300px");
		
		VerticalLayout uploadLayout = new VerticalLayout();
		uploadLayout.addComponent(sessionName);
		uploadLayout.addComponent(upload);
		uploadLayout.setMargin(true);
		//uploadLayout.setSpacing(true);
		
		//create grid
		
		dss = DataServerService.get();
		ontologies = new BeanItemContainer<Ontology>(Ontology.class);
		long user_id = (long)VaadinSession.getCurrent().getSession().getAttribute("user_id");
		ontologies.addAll(dss.getOntologies(user_id));
		
		
		Button importButton = new Button("Import Ontology");
		importButton.addClickListener(event->{
			System.out.println("Import ontology");
			Ontology o = (Ontology)selectOntologyGrid.getSelectedRow();
			System.out.println("id is " + o.getId());
			String filedirectory = basepath + "/VAADIN/themes/ontokeeper/ontologies/";
			
			//check if directory does not exists
			if(!Files.exists(Paths.get(filedirectory), LinkOption.NOFOLLOW_LINKS)){
				try {
					Files.createDirectory(Paths.get(filedirectory));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			File fo = dss.retrieveOntologyFile(o.getId(), filedirectory);
			
			Notification success = new Notification("Your ontology file has been loaded", 
					Notification.Type.TRAY_NOTIFICATION);
			success.setDelayMsec(2000);
			success.show(Page.getCurrent());
			
			ProjectProfile p = (ProjectProfile)VaadinSession.getCurrent().getSession().getAttribute("ProjectProfile");
			p.setFileLocation(fo.getAbsolutePath());
			p.setOntology_ID(o.getId());
			
			OntologyService os = (OntologyService)VaadinSession.getCurrent().getSession().getAttribute("OntologyService");
			os.loadOntologyFile(fo.getAbsolutePath(), p.isUsingUniqueIds());
			
			try {
				os.init();
			} catch (OWLOntologyCreationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			ontologyLabel.setCaption(fo.getName());
			removebutton.setEnabled(true);
			
			//NLGService nlg = NLGService.get(); //load existing 
			//nlg.setOntology(fo.getAbsolutePath());
			
			//dss.addGeneratedStatements(o.getId(), nlg.getNLStatements());
			//dss.retrieveOntologyFile(index)
		});
		
		Button deleteButton = new Button("Delete Ontology");
		deleteButton.addClickListener(event->{
			System.out.println("Delete ontology");
			Ontology o = (Ontology)selectOntologyGrid.getSelectedRow();
			System.out.println("Selected id: " + o.getId());
			dss.deleteOntology(o.getId());
			
			//update the grid throuogh the data source
			ontologies.removeAllItems();
			
			ontologies.addAll(dss.getOntologies(user_id));
		
			
		});
		
		Button refreshGrid = new Button ("Refresh Grid");
		refreshGrid.addClickListener(event->{
			//update the grid throuogh the data source
			ontologies.removeAllItems();
			ontologies.addAll(dss.getOntologies(user_id));
		});
		refreshGrid.setStyleName("refresh-grid-button");
		
		selectOntologyGrid = new Grid("Your ontologies");
		//selectOntologyGrid.set
		selectOntologyGrid.setContainerDataSource(ontologies);
		selectOntologyGrid.setWidth("100%");
		selectOntologyGrid.setHeight("100%");
		selectOntologyGrid.setResponsive(true);
		selectOntologyGrid.removeColumn("user_id");
		selectOntologyGrid.removeColumn("filepath");
		selectOntologyGrid.removeColumn("id");
		selectOntologyGrid.getColumn("filename").setHeaderCaption("File Name");
		selectOntologyGrid.getColumn("sessiondate").setHeaderCaption("Date");
		selectOntologyGrid.getColumn("evaluationsession").setHeaderCaption("Session Name");
		selectOntologyGrid.addAttachListener(event->{
			//System.out.println("Selected");
		});
		
		
		
		VerticalLayout selectLayout = new VerticalLayout();
		selectLayout.setMargin(true);
		selectLayout.setSpacing(true);
		selectLayout.addComponent(refreshGrid); 
		selectLayout.addComponent(selectOntologyGrid);
		selectLayout.addComponent(importButton);
		selectLayout.addComponent(deleteButton);
		
		
		
		fileUses.addTab(uploadLayout, "Upload Ontology");
		fileUses.addTab(selectLayout, "Select an ontology");
		fileUses.addSelectedTabChangeListener(event->{
			if(event.getTabSheet().getTabIndex()==2){
				System.out.println("refreshed");
				ontologies.removeAllItems();
				ontologies.addAll(dss.getOntologies(user_id));
			}
		});
		
		
		
		
		VerticalLayout exclusion = new VerticalLayout();
		exclusion.setSpacing(true);
		exclusion.setMargin(true);

		
		cbSyntactic.addValueChangeListener(this);
		cbSyntactic.setId("syntactic");
		exclusion.addComponent(cbSyntactic);

		
		cbSemantic.addValueChangeListener(this);
		cbSemantic.setId("semantic");
		exclusion.addComponent(cbSemantic);

		
		cbPragmatic.addValueChangeListener(this);
		cbPragmatic.setId("pragmatic");
		exclusion.addComponent(cbPragmatic);

		
		cbSocial.addValueChangeListener(this);
		cbSocial.setId("social");
		exclusion.addComponent(cbSocial);

		organizer.addTab(exclusion, "Excluded Aspects");

		

		VerticalLayout parsing = new VerticalLayout();
		parsing.setSpacing(true);
		parsing.setMargin(true);

		
		cbCamelCase.addValueChangeListener(this);
		cbCamelCase.setId("camel-case");
		parsing.addComponent(cbCamelCase);

		
		cbDeterminers.addValueChangeListener(this);
		cbDeterminers.setId("determiters");
		parsing.addComponent(cbDeterminers);

		
		cbBrackets.addValueChangeListener(this);
		cbBrackets.setId("brackets");
		parsing.addComponent(cbBrackets);

		
		cbUnderscores.addValueChangeListener(this);
		cbUnderscores.setId("underscores");
		parsing.addComponent(cbUnderscores);

		
		cbDashes.addValueChangeListener(this);
		cbDashes.setId("dashes");
		parsing.addComponent(cbDashes);

		
		/*cbLabeling.addValueChangeListener(this);
		cbLabeling.setId("unique-labels");
		parsing.addComponent(cbLabeling);*/

		organizer.addTab(parsing, "Parsing Options");
		
		

		this.setSpacing(true);

		this.addComponent(fileUses);
		
		addInfoPanel();
		//this.addComponent(new Label("<hr>", ContentMode.HTML));
		this.addComponent(organizer);

	}


	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		p.setBrackets(cbBrackets.getValue());
		p.setCamelCase(cbCamelCase.getValue());
		p.setDetermiters(cbDeterminers.getValue());
		p.setUnderscore(cbUnderscores.getValue());
		p.setDashes(cbDashes.getValue());
		//p.setUsingUniqueIds(cbLabeling.getValue());
		
		ss.pragmaticModel.setExcluded(cbPragmatic.getValue());
		ss.semanticModel.setExcluded(cbSemantic.getValue());
		ss.syntacitModel.setExcluded(cbSyntactic.getValue());
		ss.socialModel.setExcluded(cbSocial.getValue());
	}

	class OntologyUploader implements Receiver, SucceededListener {
		public File file;

		@Override
		public OutputStream receiveUpload(String filename,
				String mimeType) {
			// Create upload stream
			FileOutputStream fos = null; // Stream to write to
			try {
				// Open the file for writing.
				
				//check if directory exists first
				String ontoDirectory = basepath + "/VAADIN/themes/ontokeeper/ontologies/";
				
				if(!Files.exists(Paths.get(ontoDirectory), LinkOption.NOFOLLOW_LINKS)){
					try {
						Files.createDirectory(Paths.get(ontoDirectory));
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				
				file = new File(ontoDirectory + filename);
				fos = new FileOutputStream(file);
				//System.out.println(file.getAbsolutePath());

			} catch (final java.io.FileNotFoundException e) {
				new Notification("Could not open file",
						e.getMessage(),
						Notification.Type.ERROR_MESSAGE)
				.show(Page.getCurrent());
				return null;
			}
			return fos; // Return the output stream to write to
		}
		@Override
		public void uploadSucceeded(SucceededEvent event) {

			Notification success = new Notification("Your ontology file has been uploaded", 
					Notification.Type.HUMANIZED_MESSAGE);

			success.setDelayMsec(20000);
			success.show(Page.getCurrent());

			ProjectProfile p = (ProjectProfile)VaadinSession.getCurrent().getSession().getAttribute("ProjectProfile");
			p.setFileLocation(file.getAbsolutePath());
			
			OntologyService os = (OntologyService)VaadinSession.getCurrent().getSession().getAttribute("OntologyService");
			os.loadOntologyFile(file.getAbsolutePath(), p.isUsingUniqueIds());
			
			NLGService nlg = NLGService.get();
			nlg.setOntology(file.getAbsolutePath());
			
			//save ontology to database
			DataServerService dss = DataServerService.get();
			long ontoKey;
			
			String session_name = "";
			if(sessionName.getValue() == ""){
				session_name = "NA";
			}
			else{
				session_name = sessionName.getValue();
			}
			
			if((ontoKey = dss.saveOntologyFile(file, session_name, user_id))>0){
				//System.out.println("Successful save");
				p.setOntology_ID(ontoKey);
				
				dss.addGeneratedStatements(ontoKey, nlg.getNLStatements());
			}

			
			try {
				os.init();
			} catch (OWLOntologyCreationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			//ontologyLabel.setCaption(file.getName());
			
			ontologyLabel.setCaption(file.getName());
			removebutton.setEnabled(true);
		}
		


	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		// TODO Auto-generated method stub
		//System.out.println(((CheckBox)event.getProperty()).getCaption());
		String id = ((CheckBox)event.getProperty()).getId();
		
		switch(id){
		case "underscores": p.setUnderscore(((CheckBox)event.getProperty()).getValue());break;
		case "camel-case":p.setCamelCase(((CheckBox)event.getProperty()).getValue());break;
		case "determiters":p.setDetermiters(((CheckBox)event.getProperty()).getValue());break;
		case "brackets":p.setBrackets(((CheckBox)event.getProperty()).getValue());break;
		case "dashes":p.setDashes(((CheckBox)event.getProperty()).getValue());break;
		case "unique-labels":p.setUsingUniqueIds(((CheckBox)event.getProperty()).getValue());break;
		case "syntactic" : ss.syntacitModel.setExcluded(((CheckBox)event.getProperty()).getValue()); break;
		case "semantic" : ss.semanticModel.setExcluded(((CheckBox)event.getProperty()).getValue()); break;
		case "pragmatic" : ss.pragmaticModel.setExcluded(((CheckBox)event.getProperty()).getValue()); break;
		case "social" : ss.socialModel.setExcluded(((CheckBox)event.getProperty()).getValue()); break;
		default: break;
		
		}
		
		

	};

}
