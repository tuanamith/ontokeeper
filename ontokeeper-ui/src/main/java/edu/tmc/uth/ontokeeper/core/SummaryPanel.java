package edu.tmc.uth.ontokeeper.core;

import java.text.DecimalFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.vaadin.addons.d3Gauge.Gauge;
import org.vaadin.addons.d3Gauge.GaugeConfig;
import org.vaadin.addons.d3Gauge.GaugeStyle;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Slider;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import edu.tmc.uth.ontokeeper.ProjectProfile;
import edu.tmc.uth.ontokeeper.SEMSService;
import edu.tmc.uth.ontokeeper.backend.DataServerService;
import edu.tmc.uth.ontokeeper.data.PragmaticModel;
import edu.tmc.uth.ontokeeper.data.SemanticModel;
import edu.tmc.uth.ontokeeper.data.SyntacticModel;

public class SummaryPanel extends PanelContent{

	public static final String VIEW_NAME = "Summary";
	Label overallScore;
	Gauge syntacticGauge = null;
	Gauge semanticGauge = null;
	Gauge pragmaticGauge = null;
	Gauge socialGauge = null;

	SEMSService ss = null;
	PragmaticModel pm = null;
	SemanticModel sem = null;
	SyntacticModel sym = null;
	
	Slider sSyntactic = null;
	Slider sPragmatic = null;
	Slider sSemantic = null;
	Slider sSocial = null;
	
	private DataServerService dss = null;
	private Map<String,String>snapshotData = null;
	
	TextField saveName = null;

	public SummaryPanel() {
		// TODO Auto-generated constructor stub

		this.createTitle("Summary");
		this.createSubtitle("Aggregrate data of the scores");
		initUI();
	}

	private void initUI() {
		// TODO Auto-generated method stub

		overallScore = new Label("0.00");
		overallScore.setStyleName("h1 score-label");



		this.addComponent(overallScore);
		
		if(ss == null){
			ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
			pm = ss.pragmaticModel;
			sem = ss.semanticModel;
			sym = ss.syntacitModel;
		}

		insertGauges();
		insertSummary();
		insertWeightSliders();
		

	}

	private void insertWeightSliders() {
		// TODO Auto-generated method stub

		Panel weightPanel = new Panel("Adjust weights of the scores for each of the scores");
		weightPanel.setSizeFull();

		//add content
		VerticalLayout weightLayout = new VerticalLayout();
		weightLayout.setSpacing(true);
		weightLayout.setMargin(true);

		//Weight Syntactic Widget
		HorizontalLayout sSyntacticWidget = new HorizontalLayout();
		sSyntacticWidget.setSpacing(true);
		sSyntacticWidget.setSizeFull();
		sSyntactic = new Slider(0,1,2);
		sSyntactic.setCaption("Weight of Syntactic");
		sSyntactic.setValue(ss.getWeight_syntactic());
		sSyntactic.setWidth("100%");
		Label sSyntacticLabel = new Label(Double.toString(ss.getWeight_syntactic()));
		sSyntacticLabel.setStyleName("h2");
		

		sSyntacticWidget.addComponent(sSyntactic);
		sSyntacticWidget.addComponent(sSyntacticLabel);
		weightLayout.addComponent(sSyntacticWidget);

		//Weight Semantic Widget
		HorizontalLayout sSemanticWidget = new HorizontalLayout();
		sSemanticWidget.setSpacing(true);
		sSemanticWidget.setSizeFull();
		sSemantic = new Slider(0,1,2);
		sSemantic.setCaption("Weight of Semantic");
		sSemantic.setValue(ss.getWeight_semantic());
		sSemantic.setWidth("100%");
		Label sSemanticLabel = new Label(Double.toString(ss.getWeight_semantic()));
		sSemanticLabel.setStyleName("h2");

		sSemanticWidget.addComponent(sSemantic);
		sSemanticWidget.addComponent(sSemanticLabel);
		weightLayout.addComponent(sSemanticWidget);

		//Weight Pragmatic Widget
		HorizontalLayout sPragmaticWidget = new HorizontalLayout();
		sPragmaticWidget.setSpacing(true);
		sPragmaticWidget.setSizeFull();
		sPragmatic = new Slider(0,1,2);
		sPragmatic.setCaption("Weight of Pragmatic");
		sPragmatic.setValue(ss.getWeight_pragmatic());
		sPragmatic.setWidth("100%");
		Label sPragmaticLabel = new Label(Double.toString(ss.getWeight_pragmatic()));
		sPragmaticLabel.setStyleName("h2");

		sPragmaticWidget.addComponent(sPragmatic);
		sPragmaticWidget.addComponent(sPragmaticLabel);
		weightLayout.addComponent(sPragmaticWidget);

		//Weight Social Widget
		HorizontalLayout sSocialWidget = new HorizontalLayout();
		sSocialWidget.setSpacing(true);
		sSocialWidget.setSizeFull();
		sSocial = new Slider(0,1,2);
		sSocial.setCaption("Weight of Social");
		sSocial.setValue(ss.getWeight_social());
		sSocial.setWidth("100%");
		Label sSocialLabel = new Label(Double.toString(ss.getWeight_social()));
		sSocialLabel.setStyleName("h2");

		sSocialWidget.addComponent(sSocial);
		sSocialWidget.addComponent(sSocialLabel);
		weightLayout.addComponent(sSocialWidget);
		sSocialWidget.setEnabled(false);//social is not supported yet

		weightPanel.setContent(weightLayout);
		this.addComponent(weightPanel);
		
		//add action listener
		
		sSyntactic.addValueChangeListener(ev->{
			double valueCheck = 0.00;
			valueCheck = (double)ev.getProperty().getValue() + (double)sSemantic.getValue() + (double)sPragmatic.getValue()+(double)sSocial.getValue();
			
			if(valueCheck ==1 || valueCheck ==0.99){
				
				if(ss == null){
					ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
				}
				
				ss.setWeight_syntactic((double)ev.getProperty().getValue());
				
				sSyntacticLabel.setValue(Double.toString((double)ev.getProperty().getValue()));
				
				sSyntacticLabel.setStyleName("h2");
				sSemanticLabel.setStyleName("h2");
				sPragmaticLabel.setStyleName("h2");
				sSocialLabel.setStyleName("h2");
				
				//this.refreshScore();
				
				//REFRESH SCORES
				DecimalFormat df = new DecimalFormat("#.##");
				
				
				pm = ss.pragmaticModel;
				sem = ss.semanticModel;
				sym = ss.syntacitModel;
				
				
				double pragmatic = pm.getPragmaticScore();
				double semantic = sem.getSemanticScore();
				double syntactic = sym.getSyntacticScore();
				
				
				double overall = ((pragmatic*sPragmatic.getValue())+(semantic*sSemantic.getValue())+(syntactic*(double)ev.getProperty().getValue()));
				
				pragmaticGauge.setValue((int)(pragmatic*100));
				semanticGauge.setValue((int)(semantic*100));
				syntacticGauge.setValue((int)(syntactic*100));

				overallScore.setValue(df.format(overall));
				
				
				
			}
			else{
				
				sSyntacticLabel.setValue(Double.toString((double)ev.getProperty().getValue()));
				
				sSyntacticLabel.setStyleName("h2 weight-issue");
				sSemanticLabel.setStyleName("h2 weight-issue");
				sPragmaticLabel.setStyleName("h2 weight-issue");
				sSocialLabel.setStyleName("h2 weight-issue");
				
				Notification notif = new Notification("NOTE:\nWeights won't propogate unless values are distrubuted equally", Notification.Type.WARNING_MESSAGE);
				notif.setDelayMsec(2000);
				notif.setPosition(Position.MIDDLE_CENTER);
				notif.show(Page.getCurrent());
				
			}
		});
		
		
		sSemantic.addValueChangeListener(ev->{
			double valueCheck = 0.00;
			valueCheck = (double)ev.getProperty().getValue() + (double)sSyntactic.getValue() + (double)sPragmatic.getValue()+(double)sSocial.getValue();
			
			if(valueCheck ==1 || valueCheck ==0.99){
				
				if(ss == null){
					ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
				}
				
				ss.setWeight_semantic((double)ev.getProperty().getValue());
				
				sSemanticLabel.setValue(Double.toString((double)ev.getProperty().getValue()));
				
				sSyntacticLabel.setStyleName("h2");
				sSemanticLabel.setStyleName("h2");
				sPragmaticLabel.setStyleName("h2");
				sSocialLabel.setStyleName("h2");
				
				//this.refreshScore();
				//REFRESH SCORE
				
				DecimalFormat df = new DecimalFormat("#.##");
				
				pm = ss.pragmaticModel;
				sem = ss.semanticModel;
				sym = ss.syntacitModel;
				
				double pragmatic = pm.getPragmaticScore();
				double semantic = sem.getSemanticScore();
				double syntactic = sym.getSyntacticScore();
				
				
				double overall = ((pragmatic*sPragmatic.getValue())+(semantic*(double)ev.getProperty().getValue())+(syntactic*sSyntactic.getValue()));
				
				pragmaticGauge.setValue((int)(pragmatic*100));
				semanticGauge.setValue((int)(semantic*100));
				syntacticGauge.setValue((int)(syntactic*100));

				overallScore.setValue(df.format(overall));
				
				
			}
			else{
				
				sSemanticLabel.setValue(Double.toString((double)ev.getProperty().getValue()));
				
				sSyntacticLabel.setStyleName("h2 weight-issue");
				sSemanticLabel.setStyleName("h2 weight-issue");
				sPragmaticLabel.setStyleName("h2 weight-issue");
				sSocialLabel.setStyleName("h2 weight-issue");
				
				Notification notif = new Notification("NOTE:\nWeights won't propogate unless values are distrubuted equally", Notification.Type.WARNING_MESSAGE);
				notif.setDelayMsec(2000);
				notif.setPosition(Position.MIDDLE_CENTER);
				notif.show(Page.getCurrent());
				
			}
		});
		
		sPragmatic.addValueChangeListener(ev->{
			double valueCheck = 0.00;
			valueCheck = (double)ev.getProperty().getValue() + (double)sSyntactic.getValue() + (double)sSemantic.getValue()+(double)sSocial.getValue();
			
			if(valueCheck ==1 || valueCheck ==0.99){
				
				if(ss == null){
					ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
				}
				
				ss.setWeight_pragmatic((double)ev.getProperty().getValue());
				
				sPragmaticLabel.setValue(Double.toString((double)ev.getProperty().getValue()));
				
				sSyntacticLabel.setStyleName("h2");
				sSemanticLabel.setStyleName("h2");
				sPragmaticLabel.setStyleName("h2");
				sSocialLabel.setStyleName("h2");
				
				//this.refreshScore();
				
				//REFRESH SCORE
				
				DecimalFormat df = new DecimalFormat("#.##");
				
				pm = ss.pragmaticModel;
				sem = ss.semanticModel;
				sym = ss.syntacitModel;
				
				double pragmatic = pm.getPragmaticScore();
				double semantic = sem.getSemanticScore();
				double syntactic = sym.getSyntacticScore();
				
				
				double overall = ((pragmatic*(double)ev.getProperty().getValue())+(semantic*(double)sSemantic.getValue())+(syntactic*sSyntactic.getValue()));
				
				pragmaticGauge.setValue((int)(pragmatic*100));
				semanticGauge.setValue((int)(semantic*100));
				syntacticGauge.setValue((int)(syntactic*100));

				overallScore.setValue(df.format(overall));
				
				
			}
			else{
				
				sPragmaticLabel.setValue(Double.toString((double)ev.getProperty().getValue()));
				
				sSyntacticLabel.setStyleName("h2 weight-issue");
				sSemanticLabel.setStyleName("h2 weight-issue");
				sPragmaticLabel.setStyleName("h2 weight-issue");
				sSocialLabel.setStyleName("h2 weight-issue");
				
				Notification notif = new Notification("NOTE:\nWeights won't propogate unless values are distrubuted equally", 
						Notification.Type.WARNING_MESSAGE);
				notif.setDelayMsec(2000);
				notif.setPosition(Position.MIDDLE_CENTER);
				notif.show(Page.getCurrent());
				
			}
		});
		
		
	}

	private void refreshScore() {
		// TODO Auto-generated method stub
		DecimalFormat df = new DecimalFormat("#.##");
		
		if(ss == null){
			ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
			
		}
		pm = ss.pragmaticModel;
		sem = ss.semanticModel;
		sym = ss.syntacitModel;
		
		double pragmatic = pm.getPragmaticScore();
		double semantic = sem.getSemanticScore();
		double syntactic = sym.getSyntacticScore();
		
		
		double overall = ((pragmatic*sPragmatic.getValue())+(semantic*sSemantic.getValue())+(syntactic*sSyntactic.getValue()));
		
		pragmaticGauge.setValue((int)(pragmatic*100));
		semanticGauge.setValue((int)(semantic*100));
		syntacticGauge.setValue((int)(syntactic*100));

		overallScore.setValue(df.format(overall));
		System.out.println("Scores should refresh");
	}

	private void insertSummary() {
		// TODO Auto-generated method stub
		Panel summaryPanel = new Panel("Save Snapshot");
		//summaryPanel.setSizeUndefined();
		summaryPanel.setSizeFull();
		
		HorizontalLayout hl = new HorizontalLayout();
		hl.setSpacing(true);
		hl.setMargin(true);
		
		saveName = new TextField();
		saveName.setInputPrompt("Provide snapshot name");
		
		saveName.setSizeFull();
		hl.addComponent(saveName);
		
		Button saveSession = new Button("Save");
		saveSession.addClickListener(event->{
			if(dss == null){
				dss = DataServerService.get();
			}
			
			
			
			if(!saveName.getValue().isEmpty() || !(saveName.getValue()=="")){
				
				if(snapshotData ==null){
					snapshotData = new HashMap<String,String>();
				}
				
				snapshotData.put("name", saveName.getValue());
				snapshotData.put("semantic_original", Double.toString(((double)this.semanticGauge.getValue()/100)));
				snapshotData.put("pragmatic_original", Double.toString(((double)this.pragmaticGauge.getValue()/100)));
				snapshotData.put("syntactic_original", Double.toString(((double)this.syntacticGauge.getValue()/100)));
				snapshotData.put("weights_semantic", this.sSemantic.getValue().toString());
				snapshotData.put("weights_syntactic", this.sSyntactic.getValue().toString());
				snapshotData.put("weights_pragmatic", this.sPragmatic.getValue().toString());
				snapshotData.put("date", new Date().toString());
				
				ProjectProfile p = (ProjectProfile)VaadinSession.getCurrent().getSession().getAttribute("ProjectProfile");
				long user_id = (long)VaadinSession.getCurrent().getSession().getAttribute("user_id");
				if(dss.saveSnapshotData((HashMap<String, String>) snapshotData, p.getOntology_ID(),user_id)==1){
					Notification successNotify = new Notification ("Success", "Snapshot of information saved", 
							Notification.Type.ASSISTIVE_NOTIFICATION);
					successNotify.setDelayMsec(2000);
					successNotify.setPosition(Position.MIDDLE_CENTER);
					successNotify.show(Page.getCurrent());
				}
				else{
					Notification successNotify = new Notification ("Saving did not succeed.", "Try again.",
							Notification.Type.ERROR_MESSAGE);
					successNotify.setDelayMsec(2000);
					successNotify.setPosition(Position.MIDDLE_CENTER);
					successNotify.show(Page.getCurrent());
				}
				
				
			}
			
			
			
			
			
		});
		hl.addComponent(saveSession);
		
		
		summaryPanel.setContent(hl);
		
		this.addComponent(summaryPanel);

	}
	


	private void insertGauges() {
		// TODO Auto-generated method stub
		HorizontalLayout hl = new HorizontalLayout();
		hl.setSpacing(true);
		hl.setSizeUndefined(); 
		

		syntacticGauge = new Gauge("Syntactic",76,200);
		hl.addComponent(syntacticGauge);

		semanticGauge = new Gauge("Semantic",76,200);
		hl.addComponent(semanticGauge);

		pragmaticGauge = new Gauge("Pragmatic",76,200);
		hl.addComponent(pragmaticGauge);

		GaugeConfig config = new GaugeConfig(); 
		config.setStyle(GaugeStyle.STYLE_DARK.toString());
		socialGauge = new Gauge("Social",0,200,config);
		socialGauge.setEnabled(false);
		hl.addComponent(socialGauge);
		
		this.addComponent(hl);

	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
		refreshScore();
		
		/*DecimalFormat df = new DecimalFormat("#.##");

		if(ss == null){
			ss = SEMSService.getInstance();
			pm = ss.pragmaticModel;
			sem = ss.semanticModel;
			sym = ss.syntacitModel;
		}


		double pragmatic = pm.getPragmaticScore();
		double semantic = sem.getSemanticScore();
		double syntactic = sym.getSyntacticScore();
		double overall = ((pragmatic/ss.getWeight_pragmatic())+(semantic/ss.getWeight_semantic())+(syntactic/ss.getWeight_syntactic()));

		pragmaticGauge.setValue((int)(pragmatic*100));
		semanticGauge.setValue((int)(semantic*100));
		syntacticGauge.setValue((int)(syntactic*100));

		overallScore.setValue(df.format(overall));*/


	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
