package edu.tmc.uth.ontokeeper;

import edu.tmc.uth.ontokeeper.data.PragmaticModel;
import edu.tmc.uth.ontokeeper.data.SemanticModel;
import edu.tmc.uth.ontokeeper.data.SocialModel;
import edu.tmc.uth.ontokeeper.data.SyntacticModel;

public class SEMSService {
	
	//private static SEMSService instance = null;
	public SocialModel socialModel = null;
	public SyntacticModel syntacitModel = null;
	public PragmaticModel pragmaticModel = null;
	public SemanticModel semanticModel = null;
	
	private int averageElements = 19969;
	
	private double weight_semantic = 0.33;
	private double weight_pragmatic = 0.33;
	private double weight_syntactic = 0.33;
	private double weight_social = 0.00;

	public SEMSService() {
		// TODO Auto-generated constructor stub
		this.socialModel = new SocialModel();
		this.syntacitModel = new SyntacticModel();
		this.pragmaticModel = new PragmaticModel();
		this.semanticModel = new SemanticModel();
		
		
	}
	
	
	
	
	public int getAverageElements() {
		return averageElements;
	}




	public void setAverageElements(int averageElements) {
		this.averageElements = averageElements;
	}

	

	//Commented out due to issue
	/*public static SEMSService getInstance(){
		if(instance == null){
			instance = new SEMSService();
		}
		
		return instance;
	}*/

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}




	public double getWeight_semantic() {
		return weight_semantic;
	}




	public void setWeight_semantic(double weight_semantic) {
		this.weight_semantic = weight_semantic;
	}




	public double getWeight_pragmatic() {
		return weight_pragmatic;
	}




	public void setWeight_pragmatic(double weight_pragmatic) {
		this.weight_pragmatic = weight_pragmatic;
	}




	public double getWeight_syntactic() {
		return weight_syntactic;
	}




	public void setWeight_syntactic(double weight_syntactic) {
		this.weight_syntactic = weight_syntactic;
	}




	public double getWeight_social() {
		return weight_social;
	}




	public void setWeight_social(double weight_social) {
		this.weight_social = weight_social;
	}

}
