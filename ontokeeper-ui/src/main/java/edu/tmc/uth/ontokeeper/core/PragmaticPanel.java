package edu.tmc.uth.ontokeeper.core;

import java.text.DecimalFormat;
import java.util.Locale;

import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Slider;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;
import com.vaadin.ui.renderers.ButtonRenderer;
import com.vaadin.ui.renderers.HtmlRenderer;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Grid;
import com.vaadin.ui.themes.ValoTheme;

import edu.tmc.uth.ontokeeper.ProjectProfile;
import edu.tmc.uth.ontokeeper.SEMSService;
import edu.tmc.uth.ontokeeper.backend.DataServerService;
import edu.tmc.uth.ontokeeper.backend.data.Volunteers;
import edu.tmc.uth.ontokeeper.data.PragmaticModel;

public class PragmaticPanel extends PanelContent {

	public static final String VIEW_NAME = "Pragmatic";
	private Label compLabel = null;
	private TextField txtAvgNumber;
	private DataServerService dss = null;

	private BeanItemContainer<Volunteers> dataSource = null;

	private Label accLabel = null;
	
	private SEMSService ss = null;
	private PragmaticModel pm = null;

	public PragmaticPanel() {
		// TODO Auto-generated constructor stub
		createTitle(this.VIEW_NAME);
		createSubtitle("Number of classes and properties");
		createTabs();
	}



	private void createTabs() {
		// TODO Auto-generated method stub

		if(ss == null){
			ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
			pm = ss.pragmaticModel;
		}

		TabSheet tabs = new TabSheet();
		tabs.addStyleName(ValoTheme.TABSHEET_FRAMED);
		tabs.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);
		tabs.setSizeFull();

		VerticalLayout tab1 = new VerticalLayout();
		tab1.setSpacing(true);
		tab1.setMargin(true);

		compLabel = new Label("0.00");
		compLabel.setStyleName("h1 score-label");
		tab1.addComponent(compLabel);

		Label headerDescription1 = new Label("Comprehensiveness measures 'domain coverage' of the ontology. "
				+ "Essentially, the more classes, properties, etc, the wider the domain that the ontology covers. "
				+ "According to Burton-Jones, etc, the maximum number of average is 500, or the average based "
				+ "on a specific ontology library");

		tab1.addComponent(headerDescription1);

		//tab1.addComponent(new Label("<strong>Average number of classes, properties, and instances</strong>", ContentMode.HTML));
		txtAvgNumber = new TextField("Average number of classes, properties, and instances");
		//txtAvgNumber.setValue("0");
		txtAvgNumber.addValueChangeListener(new Property.ValueChangeListener() {

			@Override
			public void valueChange(ValueChangeEvent event) {
				// TODO Auto-generated method stub
				//String value = (String) event.getProperty().getValue();
				//event.
				System.out.println("Event type: " +event.getClass().getName());
				SEMSService ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
				ss.setAverageElements(Integer.parseInt(event.getProperty().getValue().toString()));

				pragmatic();
				// Do something with the value
				//Notification.show("Value is: " + value);
			}
		});
		txtAvgNumber.setImmediate(true);

		tab1.addComponent(txtAvgNumber);

		tabs.addTab(tab1, "Comprehensiveness");

		VerticalLayout tab2 = new VerticalLayout();
		tab2.setMargin(true);
		tab2.setSpacing(true);

		accLabel = new Label("0.00");
		accLabel.setStyleName("h1 score-label");
		tab2.addComponent(accLabel);

		Label headerDescription2 = new Label("Accuracy reveals how factual the knowledge of the ontology. "
				+ "This requires the assistance of domain experts to examine each statement evoked from "
				+ "the ontology of whether it is true of false. The number of truthful statements "
				+ "are collected to determine the accuracy score");

		tab2.addComponent(headerDescription2);

		tab2.addComponent(new Label("<strong>Number of true statements from the ontology</strong>", ContentMode.HTML));
		Label trueStatements = new Label("0");
		tab2.addComponent(trueStatements);

		final Button previewButton = new Button("Preview Statements");
		previewButton.addClickListener(new Button.ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				// TODO Auto-generated method stub

				UI.getCurrent().getNavigator().navigateTo("Review");

			}
		});
		tab2.addComponent(previewButton);


		Panel emailPanel = new Panel("Subject Matter Volunteers");
		final VerticalLayout contentLayout = new VerticalLayout();

		HorizontalLayout buttonLayout = new HorizontalLayout();
		final Button addButton = new Button("Add");



		addButton.addClickListener(new Button.ClickListener() {

			@Override
			public void buttonClick(ClickEvent event) {
				// TODO Auto-generated method stub
				final Window dialog = new Window("Add Volunteer");
				dialog.setWidth(400.0f, Unit.PIXELS);
				dialog.setHeight(300.0f, Unit.PIXELS);
				dialog.setModal(true);

				final FormLayout content = new FormLayout();
				content.setSizeFull();
				content.setSpacing(true);
				content.setMargin(true);

				final TextField volunteerName = new TextField("Volunteer's Name", "");
				volunteerName.setWidth(100.0f, Unit.PERCENTAGE);
				content.addComponent(volunteerName);

				final TextField volunteerEmail = new TextField("Volunteer's Email", "");
				volunteerEmail.setWidth(100.0f, Unit.PERCENTAGE);
				content.addComponent(volunteerEmail);

				final HorizontalLayout hButtonDialog = new HorizontalLayout();
				hButtonDialog.setSpacing(true);
				final Button saveButton = new Button("Save");
				saveButton.addClickListener(new Button.ClickListener() {

					@Override
					public void buttonClick(ClickEvent event) {
						// TODO Auto-generated method stub

						Volunteers volunteer = new Volunteers(volunteerName.getValue(),
								volunteerEmail.getValue());

						/** BEST TO SAVE TO THE DATABASE THEN REFRESH THE DATASOURCE**/
						/*if(dataSource !=null){
							dataSource.addItem(volunteer);
						}
						else{
							dataSource = new BeanItemContainer<Volunteers>(Volunteers.class);
							dataSource.addItem(volunteer);

						}*/
						if(dss == null) dss = DataServerService.get();
						//Volunteers vData = new Volunteers();
						ProjectProfile p = (ProjectProfile)VaadinSession.getCurrent().getSession().getAttribute("ProjectProfile");
						dss.addVolunteer(volunteer, p.getOntology_ID());

						//TODO: create a table for the volunteer

						refreshDataSource();

						UI.getCurrent().removeWindow(dialog);
					}


				});


				final Button cancelButton = new Button("Cancel");
				cancelButton.addClickListener(new Button.ClickListener() {

					@Override
					public void buttonClick(ClickEvent event) {
						// TODO Auto-generated method stub
						UI.getCurrent().removeWindow(dialog);
					}
				});

				hButtonDialog.addComponent(saveButton);
				hButtonDialog.addComponent(cancelButton);


				content.addComponent(hButtonDialog);


				dialog.setContent(content); 
				UI.getCurrent().addWindow(dialog);


			}
		});


		buttonLayout.addComponent(addButton);

		buttonLayout.setMargin(true);
		buttonLayout.setSpacing(true);
		contentLayout.addComponent(buttonLayout);

		dataSource = new BeanItemContainer<Volunteers>(Volunteers.class);

		GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(dataSource);
		gpc.addGeneratedProperty("notify",new PropertyValueGenerator<String>() {

			@Override
			public String getValue(Item item, Object itemId,
					Object propertyId) {
				return "Notify"; // The caption
			}

			@Override
			public Class<String> getType() {
				return String.class;
			}
		});

		gpc.addGeneratedProperty("remove",new PropertyValueGenerator<String>() {

			@Override
			public String getValue(Item item, Object itemId,
					Object propertyId) {
				return "Remove"; // The caption
			}

			@Override
			public Class<String> getType() {
				return String.class;
			}
		});


		final Grid volunteerGrid = new Grid(gpc);
		volunteerGrid.setSizeFull();
		volunteerGrid.setSelectionMode(SelectionMode.SINGLE);


		volunteerGrid.getColumn("notify").setRenderer(
				new ButtonRenderer(e ->
				Notification.show("Email notification sent to volunteer.")
						));



		volunteerGrid.getColumn("remove").setRenderer(
				new ButtonRenderer(event ->{

					dss.deleteVolunteer(((Volunteers)event.getItemId()).getId());

					volunteerGrid.getContainerDataSource().removeItem(event.getItemId());
					Notification.show("Volunteer removed.");
				}));

		/*volunteerGrid.getColumn("url").setConverter(new Converter(){
			
		});*/

		//volunteerGrid.getColumn("url").setConverter(new TransformToURL());
		TransformToURL tf = new TransformToURL();
		volunteerGrid.getColumn("url").setRenderer(new HtmlRenderer(),tf);
		
		volunteerGrid.setColumnOrder("name", "url", "email");

		contentLayout.addComponent(volunteerGrid);


		emailPanel.setContent(contentLayout);

		tab2.addComponent(emailPanel);


		tabs.addTab(tab2, "Accuracy");


		VerticalLayout tab3 = new VerticalLayout();
		tab3.setMargin(true);
		tab3.setSpacing(true);

		Label headerScore3 = new Label("0.00");
		headerScore3.setStyleName("h1 score-label");
		tab3.addComponent(headerScore3);

		Label headerDescription3 = new Label("Relevance of information for a task");
		tab3.addComponent(headerDescription3);

		tab3.addComponent(new Label("<strong>Number of correct tasks</strong>", ContentMode.HTML));
		Label correctTasks = new Label("0");
		tab3.addComponent(correctTasks);


		tabs.addTab(tab3, "Relevancy");

		this.addComponent(tabs);

		//add weight adjustment widgets
		Panel weightPanel = new Panel("Adjust weights of the scores for pragmatic");
		weightPanel.setSizeFull();

		//add content
		VerticalLayout weightLayout = new VerticalLayout();
		weightLayout.setSpacing(true);
		weightLayout.setMargin(true);


		//Weight Comphrensiveness Widget
		HorizontalLayout sCompWidget = new HorizontalLayout();
		sCompWidget.setSpacing(true);
		sCompWidget.setSizeFull();
		Slider sComp = new Slider(0,1,2);
		sComp.setCaption("Weight of Comphrensiveness");
		sComp.setValue(pm.getWeight_comprehensiveness());
		sComp.setWidth("100%");
		Label sCompLabel = new Label(Double.toString(pm.getWeight_comprehensiveness()));
		sCompLabel.setStyleName("h2");

		sCompWidget.addComponent(sComp);
		sCompWidget.addComponent(sCompLabel);
		weightLayout.addComponent(sCompWidget);

		//Weight Accuracy Widget
		HorizontalLayout sAccuracyWidget = new HorizontalLayout();
		sAccuracyWidget.setSpacing(true);
		sAccuracyWidget.setSizeFull();
		Slider sAccuracy = new Slider(0,1,2);
		sAccuracy.setCaption("Weight of Accuracy");
		sAccuracy.setValue(pm.getWeight_accuracy());
		sAccuracy.setWidth("100%");
		Label sAccuracyLabel = new Label(Double.toString(pm.getWeight_accuracy()));
		sAccuracyLabel.setStyleName("h2");

		sAccuracyWidget.addComponent(sAccuracy);
		sAccuracyWidget.addComponent(sAccuracyLabel);
		weightLayout.addComponent(sAccuracyWidget);

		//Weight Relevancy Widget
		HorizontalLayout sRelevancyWidget = new HorizontalLayout();
		sRelevancyWidget.setSpacing(true);
		sRelevancyWidget.setSizeFull();
		Slider sRelevancy = new Slider(0,1,2);
		sRelevancy.setCaption("Weight of Relevancy");
		sRelevancy.setValue(pm.getWeight_relevancy());
		sRelevancy.setWidth("100%");
		Label sRelevancyLabel = new Label(Double.toString(pm.getWeight_relevancy()));
		sRelevancyLabel.setStyleName("h2");

		sRelevancyWidget.addComponent(sRelevancy);
		sRelevancyWidget.addComponent(sRelevancyLabel);
		weightLayout.addComponent(sRelevancyWidget);
		sRelevancyWidget.setEnabled(false);


		weightPanel.setContent(weightLayout);
		this.addComponent(weightPanel);

		//actionListeners

		sComp.addValueChangeListener(ev->{

			//adjust the partner slider
			sAccuracy.setValue(1-(double) ev.getProperty().getValue());


			//update object of sytnaticModel with weight
			if(pm == null){
				ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
				pm = ss.pragmaticModel;
			}

			//System.out.println(ev.getProperty().getValue());
			pm.setWeight_comprehensiveness((double)ev.getProperty().getValue());
			sCompLabel.setValue(Double.toString((double) ev.getProperty().getValue()));

		});
		
		sAccuracy.addValueChangeListener(ev->{

			//adjust the partner slider
			sComp.setValue(1-(double) ev.getProperty().getValue());


			//update object of sytnaticModel with weight
			if(pm == null){
				ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
				pm = ss.pragmaticModel;
			}

			//System.out.println(ev.getProperty().getValue());
			pm.setWeight_accuracy((double)ev.getProperty().getValue());
			sAccuracyLabel.setValue(Double.toString((double) ev.getProperty().getValue()));

		});


	}

	class EmailPanel extends Panel{



	}

	final private void refreshDataSource() {
		// TODO Auto-generated method stub

		if(dataSource.size()>0){
			dataSource.removeAllItems();
		}

		if(dss == null) dss = DataServerService.get();

		ProjectProfile p = (ProjectProfile)VaadinSession.getCurrent().getSession().getAttribute("ProjectProfile");
		
		dataSource.addAll(dss.getVolunteers(p.getOntology_ID()));


	}



	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub

		if(dataSource.size()>0){
			dataSource.removeAllItems();
		}
		if(dss == null) dss = DataServerService.get();

		ProjectProfile p = (ProjectProfile)VaadinSession.getCurrent().getSession().getAttribute("ProjectProfile");
		dataSource.addAll(dss.getVolunteers(p.getOntology_ID()));

		//System.out.println("Event type: " +event.getClass().getName());
		this.pragmatic();
		
		//System.out.println(dss.averageEvaluatedStatements());
	}

	public void pragmatic(){

		if(ss == null){
			ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
			pm = ss.pragmaticModel;
		}


		DecimalFormat df = new DecimalFormat("#.##");

		int avg_elements = 0;
		avg_elements = ss.getAverageElements();
		txtAvgNumber.setValue(Integer.toString(avg_elements));

		double comp = 0.0;
		comp = pm.getComprehensiveness(avg_elements);

		if(comp>0){
			compLabel.setValue(df.format(comp));
		}
		
		double acc = 0.00;
		acc = pm.getAccuracy();
		
		if(acc>0){
			accLabel.setValue(df.format(acc));
		}

	}
	
	
	class TransformToURL implements Converter<String,String>{

		@Override
		public String convertToModel(String value, Class<? extends String> targetType, Locale locale)
				throws com.vaadin.data.util.converter.Converter.ConversionException {
			// TODO Auto-generated method stub
			return "not implemented";
		}

		@Override
		public String convertToPresentation(String value, Class<? extends String> targetType, Locale locale)
				throws com.vaadin.data.util.converter.Converter.ConversionException {
			// TODO Auto-generated method stub
			String baseURL =UI.getCurrent().getPage().getLocation().toString();
			String fragment = UI.getCurrent().getPage().getLocation().getFragment();
			baseURL = baseURL.replaceAll(fragment, "");
			baseURL = baseURL.replaceAll("#", "");
			
			//append base url to the value and create html 
			return "<a href='" + baseURL.concat(value)+"' target='_blank'>" + "Click to view link" + "</a>";
		}

		@Override
		public Class<String> getModelType() {
			// TODO Auto-generated method stub
			return String.class;
		}

		@Override
		public Class<String> getPresentationType() {
			// TODO Auto-generated method stub
			return String.class;
		}
		
	}

}
