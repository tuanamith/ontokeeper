package edu.tmc.uth.ontokeeper.data;

import com.vaadin.server.VaadinSession;

import edu.tmc.uth.ontokeeper.OntologyService;
import edu.tmc.uth.ontokeeper.WordNetService;

public class SemanticModel extends SEMSAspectQuality {
	
	private double interpretability = 0.00;
	private double consistency = 0.00;
	private double clarity = 0.00;
	
	private double weight_interpretability = 0.33;
	private double weight_consistency = 0.33;
	private double weight_clarity = 0.33;

	public SemanticModel() {
		// TODO Auto-generated constructor stub
	}

	public double getInterpretability() {
		WordNetService ws = (WordNetService)VaadinSession.getCurrent().getSession().getAttribute("WordNetService");
		//interpretability = (double)ws.getTermsWithSenses()/(double)ws.getTotalSenses();
		
		interpretability = (double)ws.getTermsWithSenses()/(double)ws.totalTerms();
		
		
		if(Double.isNaN(interpretability)) return 0.00;
		else return interpretability;
		
	}
	
	public double getSemanticScore(){
		double semanticScore = 0.00;
		
		semanticScore = ((this.getInterpretability()*weight_interpretability) + 
				(this.getConsistency()*weight_consistency) + (this.getClarity()*weight_clarity));
		//semanticScore = semanticScore/3.00;
		
		return semanticScore;
	}
	
	public int termsWithSenses(){
		int total = 0;
		WordNetService ws = (WordNetService)VaadinSession.getCurrent().getSession().getAttribute("WordNetService");
		total = ws.getTermsWithSenses();
		return total;
		
	}
	
	public int totalSenses(){
		int total = 0;
		WordNetService ws = (WordNetService)VaadinSession.getCurrent().getSession().getAttribute("WordNetService");
		total = ws.getTotalSenses();
		return total;
		
	}

	public void setInterpretability(double interpretability) {
		this.interpretability = interpretability;
	}

	public double getConsistency() {
		WordNetService ws = (WordNetService)VaadinSession.getCurrent().getSession().getAttribute("WordNetService");
		OntologyService os = (OntologyService)VaadinSession.getCurrent().getSession().getAttribute("OntologyService");
		
		System.out.println("Consistency:");
		System.out.println("duplicates: " + os.duplicateTerms());
		System.out.println("total terms: " + ws.totalTerms());
		
		consistency = 1-((double)os.duplicateTerms()/(double)ws.totalTerms());
		return consistency;
	}
	
	public int dupes(){
		int total = 0;
		OntologyService os = (OntologyService)VaadinSession.getCurrent().getSession().getAttribute("OntologyService");
		total = os.duplicateTerms();
		return total;
	}

	public void setConsistency(double consistency) {
		this.consistency = consistency;
	}

	public int totalTerms(){
		int total = 0;
		WordNetService ws = (WordNetService)VaadinSession.getCurrent().getSession().getAttribute("WordNetService");
		total = ws.totalTerms();
		return total;
	}

	public double getClarity() {
		WordNetService ws = (WordNetService)VaadinSession.getCurrent().getSession().getAttribute("WordNetService");
		//Clarity = Average word sense per term /number of terms 
		clarity =((double)ws.getTotalSenses()/(double)ws.totalTerms());
		//System.out.println("Clarity is " + clarity);
		clarity = 1.00- (clarity/(double)ws.totalTerms()); 
		//System.out.println("Clarity is " + clarity);
		return clarity;
	}

	public void setClarity(double clarity) {
		this.clarity = clarity;
	}

	public double getWeight_interpretability() {
		return weight_interpretability;
	}

	public void setWeight_interpretability(double weight_interpretability) {
		this.weight_interpretability = weight_interpretability;
	}

	public double getWeight_consistency() {
		return weight_consistency;
	}

	public void setWeight_consistency(double weight_consistency) {
		this.weight_consistency = weight_consistency;
	}

	public double getWeight_clarity() {
		return weight_clarity;
	}

	public void setWeight_clarity(double weight_clarity) {
		this.weight_clarity = weight_clarity;
	}

}
