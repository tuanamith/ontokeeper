package edu.tmc.uth.ontokeeper.renderers;

import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;

import de.datenhahn.vaadin.componentrenderer.FocusPreserveExtension;
import de.datenhahn.vaadin.componentrenderer.grid.ComponentGrid;
import de.datenhahn.vaadin.componentrenderer.grid.ComponentGridDecorator;
import edu.tmc.uth.ontokeeper.backend.data.NL;

import com.vaadin.server.Sizeable;

public class CustomGridComponenets {

	private static final int ROW_HEIGHT = 40;

	public CustomGridComponenets() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public static Component createPragmaticSelector(ComponentGridDecorator componentGridDecorator,  NL nl) {

		ComboBox select = new ComboBox();
		select.setWidth(200, Sizeable.Unit.PIXELS);
		select.setHeight(ROW_HEIGHT, Sizeable.Unit.PIXELS);
		select.addItems("True", "False");
		//select.addItems(Customer.Food.FISH, Customer.Food.HAMBURGER, Customer.Food.VEGETABLES);
		//select.setPropertyDataSource(new BeanItem<>(customer).getItemProperty(Customer.FOOD));
		select.addValueChangeListener(e -> {
			componentGridDecorator.refresh();
		});
		return select;
	}

}
