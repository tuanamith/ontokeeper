package edu.tmc.uth.ontokeeper.data;

import com.vaadin.server.VaadinSession;

import edu.tmc.uth.ontokeeper.OntologyService;

public class SyntacticModel extends SEMSAspectQuality{
	
	private double lawfulness = 0.00;
	private double richness = 0.00;
	
	private double weight_lawfullness = 0.50;
	private double weight_richness = 0.50;

	public SyntacticModel() {
		// TODO Auto-generated constructor stub
	}
	
	public double getSyntacticScore(){
		double syntacticScore = 0.00;
		
		syntacticScore = (this.getLawfulness()*weight_lawfullness)+(this.getRichness()*weight_richness);
		//syntacticScore = syntacticScore/2.00;
		
		return syntacticScore;
	}

	public double getLawfulness() {
		
		OntologyService os  = (OntologyService)VaadinSession.getCurrent().getSession().getAttribute("OntologyService");
		
		lawfulness = 1-((double)os.brokenRules()/(double)os.totalAxioms()); 
		
		System.out.println("checking lawfulness: " + lawfulness);
		System.out.println("broken rules: " + (double)os.brokenRules());
		System.out.println("total axioms: " + (double)os.totalAxioms());
		if(Double.isNaN(lawfulness)) lawfulness = 0.00;
		
		
		
		return lawfulness;
	}
	
	public int brokenRulesCount(){
		
		int broken_count = 0;
		OntologyService os  = (OntologyService)VaadinSession.getCurrent().getSession().getAttribute("OntologyService");
		broken_count = os.brokenRules();
		
		return broken_count;
		
	}
	
	public int axiomsCount(){
		int axiom_count = 0;
		OntologyService os  = (OntologyService)VaadinSession.getCurrent().getSession().getAttribute("OntologyService");
		axiom_count = os.totalAxioms();
		return axiom_count;
	}

	public void setLawfulness(double lawfulness) {
		this.lawfulness = lawfulness;
	}

	public double getRichness() {
		
		OntologyService os = (OntologyService)VaadinSession.getCurrent().getSession().getAttribute("OntologyService");
		
		richness = ((double)os.totalSyntacticFeaturesUsed()/(double)os.totalSyntacticFeatures());
		
		return richness;
	}
	
	public int syntacticFeaturesUsed(){
		int syntactic_features_used = 0;
		OntologyService os = (OntologyService)VaadinSession.getCurrent().getSession().getAttribute("OntologyService");
		syntactic_features_used = os.totalSyntacticFeaturesUsed();
		return syntactic_features_used;
	}

	public void setRichness(double richness) {
		this.richness = richness;
	}

	public double getWeight_lawfullness() {
		return weight_lawfullness;
	}

	public void setWeight_lawfullness(double weight_lawfullness) {
		this.weight_lawfullness = weight_lawfullness;
	}

	public double getWeight_richness() {
		return weight_richness;
	}

	public void setWeight_richness(double weight_richness) {
		this.weight_richness = weight_richness;
	}

}
