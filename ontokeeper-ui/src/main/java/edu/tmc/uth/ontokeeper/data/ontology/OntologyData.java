package edu.tmc.uth.ontokeeper.data.ontology;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class OntologyData{

	private Set <String> terms = new HashSet<String>();
	private List <String> raw_terms = new ArrayList<String>();
	private Set <String> processed_terms = new HashSet<String>();
	private Set <Statement> statements = new HashSet<Statement>();
	private List <String> rulesBroken = new ArrayList<String>();
	
	private int numClasses, numDataProperties, numObjectProperties, numInstances;
	private String fileLocation = null;
	
	private int violations=0;
	private int numStatements=0;
	private int numSyntaticUsage=0;
	private int numSyntatic=39;
	private int numAxioms = 0;

	public OntologyData(){

	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public int getTotalElements(){
		int elements = this.numClasses + this.numInstances + 
				this.numDataProperties + this.numObjectProperties;
		return elements;
	}

	public int getNumSyntatic() {
		return numSyntatic;
	}

	public void setNumSyntatic(int numSyntatic) {
		this.numSyntatic = numSyntatic;
	}

	public int getNumSyntaticUsage() {
		return numSyntaticUsage;
	}

	public void setNumSyntaticUsage(int numSyntaticUsage) {
		this.numSyntaticUsage = numSyntaticUsage;
	}

	public int getNumStatements() {
		this.numStatements = this.statements.size();
		
		return numStatements;
	}

	public void setNumStatements(int numStatements) {
		this.numStatements = numStatements;
	}

	/**
	 * @return the numAxioms
	 */
	public int getNumAxioms() {
		return numAxioms;
	}

	/**
	 * @param numAxioms the numAxioms to set
	 */
	public void setNumAxioms(int numAxioms) {
		this.numAxioms = numAxioms;
	}

	public List <String> getRulesBroken() {
		return rulesBroken;
	}
	
	public int totalBrokenRules(){
		int total = 0;
		
		total = rulesBroken.size();
		
		return total;
	}

	public void setRulesBroken(List <String> rulesBroken) {
		this.rulesBroken = rulesBroken;
	}
	
	public void addRulesBroken(String rule){
		rulesBroken.add(rule);
	}

	public int getViolations() {
		return violations;
	}

	public void setViolations(int violations) {
		this.violations = violations;
	}

	public Set <String> getTerms() {
		return terms;
	}

	public void setTerms(Set <String> terms) {
		this.terms = terms;
		
	}

	public void insertTerm(String term){
		//System.out.println("The term " + term + " was added");
		
		this.terms.add(term);
		this.raw_terms.add(term);
	}
	
	public void insertProcessedTerm(String term){
		this.processed_terms.add(term);
	}
	
	public int numDuplicateTerms(){
		int dupes = 0;
		
		HashSet <String> dupeList = new HashSet<String>();
		for(String elementTerm: raw_terms){
			if(!dupeList.add(elementTerm)){
				dupes++;
				//System.out.println("Dupe term: " + elementTerm);
			}
		}
		
		
		return dupes;
	}

	public void printTerms(){
		System.out.println("Printing terms with size of " + terms.size());
		
		Iterator<String> iterator = terms.iterator();
		
		while(iterator.hasNext()){
			String term = iterator.next();
			System.out.println(term);
		}
	}

	public int getNumClasses() {
		return numClasses;
	}

	/**
	 * @return the raw_terms
	 */
	public List<String> getRaw_terms() {
		return raw_terms;
	}

	/**
	 * @param raw_terms the raw_terms to set
	 */
	public void setRaw_terms(List<String> raw_terms) {
		this.raw_terms = raw_terms;
	}

	public void setNumClasses(int numClasses) {
		this.numClasses = numClasses;
	}

	public int getNumDataProperties() {
		return numDataProperties;
	}

	public void setNumDataProperties(int numDataProperties) {
		this.numDataProperties = numDataProperties;
	}

	public int getNumObjectProperties() {
		return numObjectProperties;
	}

	public void setNumObjectProperties(int numObjectProperties) {
		this.numObjectProperties = numObjectProperties;
	}

	public int getNumInstances() {
		return numInstances;
	}

	public void setNumInstances(int numInstances) {
		this.numInstances = numInstances;
	}

	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
		System.out.println(fileLocation);
	}

	public Set <Statement> getStatements() {
		return statements;
	}

	public void setStatements(Set <Statement> statements) {
		this.statements = statements;
	}
	
	public void insertStatement(Statement statement){
		this.statements.add(statement);
		
		
	}
	
	public void printAllStatements(){
		System.out.println("PrintAllStatements running...");
		
		for(Statement thing: statements){
			///GWT.log(thing.getRawStatement());
			System.out.println(thing.getRawStatement());
			//System.out.println("Statement: " + thing.getSubject() + " "+thing.getVerb()+" " + thing.getObject());
		}
	}

	/**
	 * @return the processed_terms
	 */
	public Set <String> getProcessed_terms() {
		return processed_terms;
	}

	/**
	 * @param processed_terms the processed_terms to set
	 */
	public void setProcessed_terms(Set <String> processed_terms) {
		this.processed_terms = processed_terms;
	}

}
