package edu.tmc.uth.ontokeeper.core;

import java.text.DecimalFormat;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Slider;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import edu.tmc.uth.ontokeeper.SEMSService;
import edu.tmc.uth.ontokeeper.data.SemanticModel;

public class SemanticPanel extends PanelContent {

	public static final String VIEW_NAME = "Semantic";

	Label interpretabilityLabel= null;
	Label consistencyLabel = null;
	Label clarityLabel = null;

	Label numTerms = null;
	Label termWithSenses = null;
	Label inconsitentTerms= null;
	Label numWordSense = null;

	SEMSService ss = null;
	SemanticModel sm = null;

	public SemanticPanel() {
		// TODO Auto-generated constructor stub

		createTitle(this.VIEW_NAME);
		createSubtitle("'Meaningfulness of terms'");
		createTabs();
	}



	private void createTabs() {
		// TODO Auto-generated method stub

		if(ss == null){
			ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
			sm = ss.semanticModel;
		}

		TabSheet tabs = new TabSheet();
		tabs.addStyleName(ValoTheme.TABSHEET_FRAMED);
		tabs.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);
		tabs.setSizeFull();

		VerticalLayout tab1 = new VerticalLayout();

		tab1.setSpacing(true);
		tab1.setMargin(true);

		interpretabilityLabel = new Label("0.00");
		interpretabilityLabel.setStyleName("h1 score-label");
		tab1.addComponent(interpretabilityLabel);

		Label headerDescription1 = new Label("Interpretability reveals the a percentage of "
				+ "concepts that have at least one term mentioned in WordNet."
				+ "Interpretability depicts whether the ontology employs meaningful terms for concepts.");

		headerDescription1.setStyleName("p");
		tab1.addComponent(headerDescription1);

		tab1.addComponent(new Label("<strong>Number of terms</strong>", ContentMode.HTML));
		numTerms = new Label("0");
		tab1.addComponent(numTerms);

		tab1.addComponent(new Label("<strong>Number of terms with senses</strong>", ContentMode.HTML));
		termWithSenses = new Label("0");
		tab1.addComponent(termWithSenses);


		tabs.addTab(tab1, "Interpretability");
		//*****///

		VerticalLayout tab2 = new VerticalLayout();
		tab2.setSpacing(true);
		tab2.setMargin(true);

		consistencyLabel = new Label("0.00");
		consistencyLabel.setStyleName("h1 score-label");
		tab2.addComponent(consistencyLabel);

		Label headerDescription2 = new Label("Consistency measures the ratio of inconsistent terms to consistent "
				+ "terms. Inconsistent terms are based on repeated use of terms for "
				+ "concepts. For example, the use of 'male' as an instance and as a 'class' concept.");

		headerDescription2.setStyleName("p");
		tab2.addComponent(headerDescription2);

		tab2.addComponent(new Label("<strong>Number of inconsitent terms</strong>", ContentMode.HTML));
		inconsitentTerms = new Label("0");
		inconsitentTerms.setStyleName("strong");
		tab2.addComponent(inconsitentTerms);


		tabs.addTab(tab2, "Consistency");

		///*****////
		VerticalLayout tab3 = new VerticalLayout();
		tab3.setSpacing(true);
		tab3.setMargin(true);

		clarityLabel = new Label("0.00");
		clarityLabel.setStyleName("h1 score-label");
		tab3.addComponent(clarityLabel);


		Label headerDescription3 = new Label("Clarity describes lack of ambiguity of the terms in the ontology. "
				+ "This is represented by the number of word senses per term for a concept. "
				+ "For example, if a term has 5 word senses versus another a term that has 3 word sense, "
				+ "the latter is less ambiguous due to the lesser amount of word senses. Word senses are calculated with WordNet, "
				+ "and multi-termed concepts aggregates the word sense for each term.");

		headerDescription3.setStyleName("p");
		tab3.addComponent(headerDescription3);

		tab3.addComponent(new Label("<strong>Number of word senses</strong>", ContentMode.HTML));
		numWordSense = new Label("0");
		numWordSense.setStyleName("strong");
		tab3.addComponent(numWordSense);


		tabs.addTab(tab3, "Clarity");

		this.addComponent(tabs);

		//add weight adjustment widgets
		Panel weightPanel = new Panel("Adjust weights of the scores for semantic");
		weightPanel.setSizeFull();

		//add content
		VerticalLayout weightLayout = new VerticalLayout();
		weightLayout.setSpacing(true);
		weightLayout.setMargin(true);


		//Weight Interpretability Widget
		HorizontalLayout sInterWidget = new HorizontalLayout();
		sInterWidget.setSpacing(true);
		sInterWidget.setSizeFull();
		Slider sInter = new Slider(0,1,2);
		sInter.setCaption("Weight of Interpretability");
		sInter.setValue(sm.getWeight_interpretability());
		sInter.setWidth("100%");
		Label sInterLabel = new Label(Double.toString(sm.getWeight_interpretability()));
		sInterLabel.setStyleName("h2");

		sInterWidget.addComponent(sInter);
		sInterWidget.addComponent(sInterLabel);
		weightLayout.addComponent(sInterWidget);

		//Weight Consistency Widget
		HorizontalLayout sConsistWidget = new HorizontalLayout();
		sConsistWidget.setSpacing(true);
		sConsistWidget.setSizeFull();
		Slider sConsist = new Slider(0,1,2);
		sConsist.setCaption("Weight of Consistency");
		sConsist.setValue(sm.getWeight_consistency());
		sConsist.setWidth("100%");
		Label sConsistLabel = new Label(Double.toString(sm.getWeight_consistency()));
		sConsistLabel.setStyleName("h2");

		sConsistWidget.addComponent(sConsist);
		sConsistWidget.addComponent(sConsistLabel);
		weightLayout.addComponent(sConsistWidget);

		//Weight Clarity Widget
		HorizontalLayout sClarityWidget = new HorizontalLayout();
		sClarityWidget.setSpacing(true);
		sClarityWidget.setSizeFull();
		Slider sClarity = new Slider(0,1,2);
		sClarity.setCaption("Weight of Clarity");
		sClarity.setValue(sm.getWeight_clarity());
		sClarity.setWidth("100%");
		Label sClarityLabel = new Label(Double.toString(sm.getWeight_clarity()));
		sClarityLabel.setStyleName("h2");

		sClarityWidget.addComponent(sClarity);
		sClarityWidget.addComponent(sClarityLabel);
		weightLayout.addComponent(sClarityWidget);

		weightPanel.setContent(weightLayout);
		this.addComponent(weightPanel);

		//add action listener

		sInter.addValueChangeListener(ev->{
			//change color of the labels (red = over, 
			double valueCheck = 0.00;

			valueCheck = 
					(double)ev.getProperty().getValue() + (double)sClarity.getValue() + (double)sConsist.getValue()
					;

			if((valueCheck==1) || (valueCheck==0.99)){
				if(sm == null){
					ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
					sm = ss.semanticModel;
				}
				sm.setWeight_interpretability((double)ev.getProperty().getValue());
				sInterLabel.setValue(Double.toString((double)ev.getProperty().getValue()));

				sInterLabel.setStyleName("h2");
				sConsistLabel.setStyleName("h2");
				sClarityLabel.setStyleName("h2");
			}
			else{
				sInterLabel.setValue(Double.toString((double)ev.getProperty().getValue()));

				sInterLabel.setStyleName("h2 weight-issue");
				sConsistLabel.setStyleName("h2 weight-issue");
				sClarityLabel.setStyleName("h2 weight-issue");


				Notification notif = new Notification(
						"NOTE:\nWeights won't propogate unless values are distributed equally",
						Notification.Type.WARNING_MESSAGE);
				notif.setDelayMsec(2000);
				notif.setPosition(Position.MIDDLE_CENTER);
				notif.show(Page.getCurrent());

			}

		});

		sConsist.addValueChangeListener(ev->{
			double valueCheck = 0.00;

			valueCheck = 
					(double)ev.getProperty().getValue() + (double)sClarity.getValue() + (double)sInter.getValue()
					;

			if((valueCheck==1) || (valueCheck==0.99)){
				if(sm == null){
					ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
					sm = ss.semanticModel;
				} 
				sm.setWeight_consistency((double)ev.getProperty().getValue());
				sConsistLabel.setValue(Double.toString((double)ev.getProperty().getValue()));

				sInterLabel.setStyleName("h2");
				sConsistLabel.setStyleName("h2");
				sClarityLabel.setStyleName("h2");
			}
			else{
				sConsistLabel.setValue(Double.toString((double)ev.getProperty().getValue()));

				sInterLabel.setStyleName("h2 weight-issue");
				sConsistLabel.setStyleName("h2 weight-issue");
				sClarityLabel.setStyleName("h2 weight-issue");


				Notification notif = new Notification(
						"NOTE:\nWeights won't propogate unless values are distributed equally",
						Notification.Type.WARNING_MESSAGE);
				notif.setDelayMsec(2000);
				notif.setPosition(Position.MIDDLE_CENTER);
				notif.show(Page.getCurrent());

			}
		});
		
		sClarity.addValueChangeListener(ev->{
			double valueCheck = 0.00;

			valueCheck = 
					(double)ev.getProperty().getValue() + (double)sInter.getValue() + (double)sConsist.getValue()
					;

			if((valueCheck==1) || (valueCheck==0.99)){
				if(sm == null){
					ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
					sm = ss.semanticModel;
				} 
				sm.setWeight_clarity((double)ev.getProperty().getValue());
				sClarityLabel.setValue(Double.toString((double)ev.getProperty().getValue()));

				sInterLabel.setStyleName("h2");
				sConsistLabel.setStyleName("h2");
				sClarityLabel.setStyleName("h2");
			}
			else{
				sClarityLabel.setValue(Double.toString((double)ev.getProperty().getValue()));

				sInterLabel.setStyleName("h2 weight-issue");
				sConsistLabel.setStyleName("h2 weight-issue");
				sClarityLabel.setStyleName("h2 weight-issue");


				Notification notif = new Notification(
						"NOTE:\nWeights won't propogate unless values are distributed equally",
						Notification.Type.WARNING_MESSAGE);
				notif.setDelayMsec(2000);
				notif.setPosition(Position.MIDDLE_CENTER);
				notif.show(Page.getCurrent());

			}
		});



	}

	private void setWarningForWeights(boolean input){

	}



	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		DecimalFormat df = new DecimalFormat("#.##");

		if(ss == null){
			ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
			sm = ss.semanticModel;
		}


		if(sm.getInterpretability()>0){
			interpretabilityLabel.setValue(df.format(sm.getInterpretability()));
		}

		if(sm.getConsistency()>0){
			consistencyLabel.setValue(df.format(sm.getConsistency()));
		}

		if(sm.getClarity()>0){
			clarityLabel.setValue(df.format(sm.getClarity()));
		}

		numTerms.setValue(Integer.toString(sm.totalTerms()));
		termWithSenses.setValue(Integer.toString(sm.termsWithSenses()));
		inconsitentTerms.setValue(Integer.toString(sm.dupes()));
		numWordSense.setValue(Integer.toString(sm.totalSenses()));
	}

}
