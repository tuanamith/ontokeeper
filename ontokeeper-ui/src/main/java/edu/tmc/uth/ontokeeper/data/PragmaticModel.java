package edu.tmc.uth.ontokeeper.data;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.server.VaadinSession;

import edu.tmc.uth.ontokeeper.OntologyService;
import edu.tmc.uth.ontokeeper.ProjectProfile;
import edu.tmc.uth.ontokeeper.SEMSService;
import edu.tmc.uth.ontokeeper.backend.DataServerService;

public class PragmaticModel extends SEMSAspectQuality {
	
	private double comprehensiveness = 0.00;
	private double accuracy = 0.00;
	private double relevancy = 0.00;
	
	private double weight_comprehensiveness = 0.50;
	private double weight_accuracy = 0.50;
	private double weight_relevancy = 0.00;

	public PragmaticModel() {
		// TODO Auto-generated constructor stub
	}

	//TODO Need to modifiy this parameter to just get a singleton.
	public double getComprehensiveness(int avg_elements) {
		OntologyService os = (OntologyService)VaadinSession.getCurrent().getSession().getAttribute("OntologyService");
		comprehensiveness = (double)os.totalElements()/(double)avg_elements;
		
		return comprehensiveness;
	}

	public void setComprehensiveness(double comprehensiveness) {
		this.comprehensiveness = comprehensiveness;
	}
	
	public double getAccuracy(){
		
		DataServerService dss = DataServerService.get();
		List<Double>a_scores = new ArrayList<Double>();
		
		ProjectProfile p = (ProjectProfile)VaadinSession.getCurrent().getSession().getAttribute("ProjectProfile");
		
		a_scores.addAll(dss.averageEvaluatedStatements(p.getOntology_ID()));
		
		double tally =0.00;
		for(Double a_score : a_scores){
			tally += a_score;
		}
		
		this.accuracy = tally/a_scores.size();
		
		
		return this.accuracy;
	}
	
	/**
	 * @param accuracy the accuracy to set
	 */
	public void setAccuracy(double accuracy) {
		this.accuracy = accuracy;
	}

	public double getPragmaticScore(){
		double pragmaticScore = 0.0;
		SEMSService ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
		double avg_elements = (double)ss.getAverageElements();
		OntologyService os = (OntologyService)VaadinSession.getCurrent().getSession().getAttribute("OntologyService");
		
		comprehensiveness = (double)os.totalElements()/(double)avg_elements;
		
		pragmaticScore = comprehensiveness;
		
		return pragmaticScore;
	}

	public double getWeight_comprehensiveness() {
		return weight_comprehensiveness;
	}

	public void setWeight_comprehensiveness(double weight_comprehensiveness) {
		this.weight_comprehensiveness = weight_comprehensiveness;
	}

	public double getWeight_accuracy() {
		return weight_accuracy;
	}

	public void setWeight_accuracy(double weight_accuracy) {
		this.weight_accuracy = weight_accuracy;
	}

	public double getWeight_relevancy() {
		return weight_relevancy;
	}

	public void setWeight_relevancy(double weight_relevancy) {
		this.weight_relevancy = weight_relevancy;
	}

	public double getRelevancy() {
		return relevancy;
	}

	public void setRelevancy(double relevancy) {
		this.relevancy = relevancy;
	}

}
