package edu.tmc.uth.ontokeeper.login;

import com.vaadin.server.VaadinSession;

import edu.tmc.uth.ontokeeper.backend.DataServerService;

/**
 * Default mock implementation of {@link AccessControl}. This implementation
 * accepts any string as a password, and considers the user "admin" as the only
 * administrator.
 */
public class BasicAccessControl implements AccessControl {

	private DataServerService dss = null;

	@Override
	public boolean signIn(String username, String password) {
		System.out.println("basic access control was used");
		//TODO: ADD DATABASE ACCESS LOGIN
		if (username == null || username.isEmpty())
			return false;

		if(dss == null){
			dss = DataServerService.get();
		}

		if(dss.canLogIn(username, password)){
			CurrentUser.set(username);
			VaadinSession.getCurrent().getSession().setAttribute("user_id", dss.getUserId(username, password));
			return true;

		}


		return false;
	}

	@Override
	public boolean isUserSignedIn() {
		return !CurrentUser.get().isEmpty();
	}

	@Override
	public boolean isUserInRole(String role) {
		if ("admin".equals(role)) {
			// Only the "admin" user is in the "admin" role
			return getPrincipalName().equals("admin");
		}

		// All users are in all non-admin roles
		return true;
	}

	@Override
	public String getPrincipalName() {
		return CurrentUser.get();
	}

}
