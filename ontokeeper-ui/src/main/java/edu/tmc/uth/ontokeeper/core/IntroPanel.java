package edu.tmc.uth.ontokeeper.core;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Page;
import com.vaadin.server.Sizeable.Unit;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.Position;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.ListSelect;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import edu.tmc.uth.ontokeeper.backend.DataServerService;

public class IntroPanel extends PanelContent {
	
	public static final String VIEW_NAME = "Introduction";
	
	TextField uName = null;
	PasswordField pWord1 = null;
	PasswordField pWord2 = null;
	
	private Map<String,String>userData = null;
	private DataServerService dss = null;
	
	VerticalLayout snapContent = null;
	
	
	private Map<String,String>userSnaps = null;
	
	//private BeanItemContainer

	public IntroPanel() {
		// TODO Auto-generated constructor stub
		
		
		initdata();
		
		createTitle(this.VIEW_NAME);
		createSubtitle("Greetings");
		
		initUI();
		
		
	}

	private void initdata() {
		// TODO Auto-generated method stub
		System.out.println("init data");
		
		long user_id = (long)VaadinSession.getCurrent().getSession().getAttribute("user_id");
		
		if(dss == null){
			dss = DataServerService.get();
		}
		
		if(userData ==null){
			userData = dss.getUserData(user_id);
		}
		
		//userData.forEach((key,value)->System.out.println("Label: " + key + " Data: " + value));
		
		if(userSnaps == null){
			userSnaps = dss.getUserSnapshotData(user_id);
		}
		
		
	}

	private void initUI() {
		// TODO Auto-generated method stub
		//System.out.println("init ui");
		
		TabSheet introTabs = new TabSheet();
		introTabs.setHeight(100.0f, Unit.PERCENTAGE);
		introTabs.addStyleName(ValoTheme.TABSHEET_FRAMED);
		introTabs.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);
		
		VerticalLayout tutorTab = new VerticalLayout();
		tutorTab.setMargin(true);
		tutorTab.setSpacing(true);
		tutorTab.setSizeFull();
		
		Label placeholder = new Label("<iframe width='560' "
				+ "height='315' src='https://www.youtube.com/embed/V9FzIiAgEfM?rel=0&amp;showinfo=0' frameborder='0' allowfullscreen></iframe>", 
			    ContentMode.HTML);
		
		Label headerVideo = new Label("Click video thumbnail for brief demonstration.");
		headerVideo.addStyleName(ValoTheme.LABEL_H3);
		headerVideo.setSizeUndefined();
		
		tutorTab.addComponent(headerVideo);
		tutorTab.addComponent(placeholder);
		
		
		FormLayout accountTab = new FormLayout();
		accountTab.setMargin(true);
		accountTab.setSpacing(true);
		accountTab.setSizeFull();
		
		uName = new TextField("Username");
		uName.setWidth("100%");
		uName.setValue(userData.get("username"));
		accountTab.addComponent(uName);
		
		pWord1 = new PasswordField("Password");
		pWord1.setWidth("100%");
		pWord1.setDescription("Do not use a shared password for your accounts. This site is still in prototype phase and cannot guarantee any secure password protection.");
		pWord1.setValue(userData.get("password"));
		accountTab.addComponent(pWord1);
		
		
		pWord2 = new PasswordField("Re-enter Password");
		pWord2.setWidth("100%");
		pWord2.setDescription("Do not use a shared password for your accounts. This site is still in prototype phase and cannot guarantee any secure password protection.");
		pWord2.setValue(userData.get("password"));
		accountTab.addComponent(pWord2);
		
		Button updateButton = new Button("Update");
		updateButton.addClickListener(event->{
			System.out.println("updating account");
			
			
			if(!pWord1.getValue().equals(pWord2.getValue())){
				Notification passwordNotify = new Notification("Password do not match!",
		                  "Re-enter the passwords again",
		                  Notification.Type.ERROR_MESSAGE);
				passwordNotify.setDelayMsec(2000);
				passwordNotify.setPosition(Position.TOP_CENTER);
				passwordNotify.show(Page.getCurrent());
			}
			else{
				//save data
				userData.put("username", uName.getValue());
				userData.put("password", pWord1.getValue());
				
				long user_id = (long)VaadinSession.getCurrent().getSession().getAttribute("user_id");
				
				dss.setUserData((HashMap<String, String>) userData, user_id);
				
				Notification successNotify = new Notification("Success", "Update successful", Notification.Type.HUMANIZED_MESSAGE);
				successNotify.setDelayMsec(2000);
				successNotify.setPosition(Position.TOP_CENTER);
				successNotify.show(Page.getCurrent());
			}
			
		});
		accountTab.addComponent(updateButton);
		
		HorizontalLayout snaps = new HorizontalLayout();
		snaps.setSpacing(true);
		snaps.setMargin(true);
		snaps.setSizeFull();
		
		
		
		ListSelect snapList = new ListSelect("Select a Saved Snapshot");
		snapList.setSizeFull();
		snapList.setNullSelectionAllowed(false);
		
		userSnaps.forEach((key,value)->{
			snapList.addItem(key);
			snapList.setItemCaption(key, value);
		});
		
		
		snapList.addValueChangeListener(new Property.ValueChangeListener() {
	        private static final long serialVersionUID = 1L;

	        @Override
	        public void valueChange(
	                com.vaadin.data.Property.ValueChangeEvent event) {
	            final String value = (String) snapList.getValue();
	            //snapList.get
	            System.out.println(value + " was selected");
	            System.out.println(snapList.getItemCaption(value));
	            
	            dss.getSnapshotDataById(Long.parseLong(value));
	            Map <String, String> snapshotData = new HashMap<String,String>();
	            snapshotData.putAll(dss.getSnapshotDataById(Long.parseLong(value)));
	            
	            double pragmatic = Double.parseDouble(snapshotData.get("pragmatic_original"));
	            double semantic = Double.parseDouble(snapshotData.get("semantic_original"));
	            double syntactic = Double.parseDouble(snapshotData.get("syntactic_original"));
	            double w_pragmatic = Double.parseDouble(snapshotData.get("weights_pragmatic"));
	            double w_semantic = Double.parseDouble(snapshotData.get("weights_semantic"));
	            double w_syntactic = Double.parseDouble(snapshotData.get("weights_syntactic"));
	            
	            double overall = (pragmatic * w_pragmatic) + (semantic * w_semantic) + (syntactic* w_syntactic);
	            
	            String content = "";
	            
	            content = content.concat("Syntactic: " + syntactic +"\n");
	            content = content.concat("\tSemantic Weight: " + w_syntactic + "\n");
	            content = content.concat("Semantic: " + semantic + "\n");
	            content = content.concat("\tSemantic Weight: " + w_semantic + "\n");
	            content = content.concat("Pragmatic: " + pragmatic + "\n");
	            content = content.concat("\tPragmatic Weight: " + w_pragmatic + "\n");
	            content = content.concat("= Overall Score: " + overall + "\n");
	            content = content.concat("Generated on : " + snapshotData.get("date") + "\n\n");
	            
	            //System.out.println(content);
	            snapContent.removeAllComponents();
	            Label lblContent = new Label(content);
	            lblContent.setContentMode(ContentMode.PREFORMATTED);
	            snapContent.addComponent(lblContent);
	            
	            //System.out.println("tester: " +snapshotData.get("weights_pragmatic"));
	            
	            //String content = "";
	        }
	    });
		
		
		
		snaps.addComponent(snapList);
		
		Panel infoSnap = new Panel("Snapshot Info");
		infoSnap.setSizeFull();
		
		snapContent = new VerticalLayout();
		snapContent.setMargin(true);
		snapContent.setSpacing(true);
		snapContent.setSizeFull();
		
		infoSnap.setContent(snapContent);
		
		
		snaps.addComponent(infoSnap);
		
		introTabs.addTab(tutorTab, "Getting Started");
		introTabs.addTab(accountTab, "Acount Information");
		introTabs.addTab(snaps, "Snapshot Evaluations");
		
		
		
		this.addComponent(introTabs);
		
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}
	
	

}
