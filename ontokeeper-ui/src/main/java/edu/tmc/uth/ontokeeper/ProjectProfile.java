package edu.tmc.uth.ontokeeper;

public class ProjectProfile {
	
	//private static ProjectProfile instance = null;
	
	private String fileLocation ="";
	private boolean isCamelCase;
	private boolean isDetermiters;
	private boolean isBrackets;
	private boolean isUnderscore;
	private boolean isDashes;
	private boolean isUsingUniqueIds;
	
	private long ontology_id;
	
	public void printSelection(){
		if(this.isCamelCase) System.out.println("Camel case is selected");
		if(this.isDetermiters) System.out.println("Determiters is selected");
		if(this.isBrackets) System.out.println("Brackets is selected");
		if(this.isUnderscore)System.out.println("Underscore is selected");
		if(this.isDashes) System.out.println("Dashes is selected");
		if(this.isUsingUniqueIds) System.out.println("Unique ids is selected");
	}
	
	/*public static ProjectProfile getInstance(){
		if(instance == null){
			instance = new ProjectProfile();
		}
		
		return instance;
	}*/

	public String getFileLocation() {
		return fileLocation;
	}

	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}

	public boolean isCamelCase() {
		return isCamelCase;
	}

	public void setCamelCase(boolean isCamelCase) {
		this.isCamelCase = isCamelCase;
	}

	public boolean isDetermiters() {
		return isDetermiters;
	}

	public void setDetermiters(boolean isDetermiters) {
		this.isDetermiters = isDetermiters;
	}

	public boolean isBrackets() {
		return isBrackets;
	}

	public void setBrackets(boolean isBrackets) {
		this.isBrackets = isBrackets;
	}

	public boolean isUnderscore() {
		return isUnderscore;
	}

	public void setUnderscore(boolean isUnderscore) {
		this.isUnderscore = isUnderscore;
	}

	public boolean isDashes() {
		return isDashes;
	}

	public void setDashes(boolean isDashes) {
		this.isDashes = isDashes;
	}

	public boolean isUsingUniqueIds() {
		return isUsingUniqueIds;
	}

	public void setUsingUniqueIds(boolean isUsingUniqueIds) {
		this.isUsingUniqueIds = isUsingUniqueIds;
	}

	public ProjectProfile() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public long getOntology_ID() {
		return ontology_id;
	}

	public void setOntology_ID(long ontology_id) {
		this.ontology_id = ontology_id;
	}

}
