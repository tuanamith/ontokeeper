package edu.tmc.uth.ontokeeper.main;

import edu.tmc.uth.ontokeeper.UIOntoKeeper;
import edu.tmc.uth.ontokeeper.core.ConfigurationPanel;
import edu.tmc.uth.ontokeeper.core.ExpertPanel;
import edu.tmc.uth.ontokeeper.core.IntroPanel;
import edu.tmc.uth.ontokeeper.core.PragmaticPanel;
import edu.tmc.uth.ontokeeper.core.ProcessPanel;
import edu.tmc.uth.ontokeeper.core.SemanticPanel;
import edu.tmc.uth.ontokeeper.core.SocialPanel;
import edu.tmc.uth.ontokeeper.core.SummaryPanel;
import edu.tmc.uth.ontokeeper.core.SyntacticPanel;
import edu.tmc.uth.ontokeeper.samples.about.AboutView;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.Page;
import com.vaadin.ui.CssLayout;
import com.vaadin.ui.HorizontalLayout;

/**
 * Content of the UI when the user is logged in.
 * 
 * 
 */
public class MainScreen extends HorizontalLayout {
	private Menu menu;

	public MainScreen(UIOntoKeeper ui) {

		setStyleName("main-screen");

		CssLayout viewContainer = new CssLayout();
		viewContainer.addStyleName("valo-content");
		viewContainer.setSizeFull();
		
		
		Navigator navigator = new Navigator(ui, viewContainer);
		
		navigator.addView("", IntroPanel.class);
		navigator.setErrorView(ErrorView.class);
		
		navigator.addView("Review", ExpertPanel.class);

		menu = new Menu(navigator);
		menu.addView(new ConfigurationPanel(), ConfigurationPanel.VIEW_NAME,
				ConfigurationPanel.VIEW_NAME, FontAwesome.EDIT);

		menu.addView(new ProcessPanel(), ProcessPanel.VIEW_NAME,
				ProcessPanel.VIEW_NAME, FontAwesome.WRENCH);
		menu.addView(new SyntacticPanel(), SyntacticPanel.VIEW_NAME,
				SyntacticPanel.VIEW_NAME, FontAwesome.EDIT);
		menu.addView(new SemanticPanel(), SemanticPanel.VIEW_NAME,
				SemanticPanel.VIEW_NAME, FontAwesome.EDIT);

		menu.addView(new PragmaticPanel(), PragmaticPanel.VIEW_NAME,
				PragmaticPanel.VIEW_NAME, FontAwesome.EDIT);

		menu.addView(new SocialPanel(), SocialPanel.VIEW_NAME,
				SocialPanel.VIEW_NAME, FontAwesome.EDIT);

		menu.addView(new SummaryPanel(), SummaryPanel.VIEW_NAME, SummaryPanel.VIEW_NAME, FontAwesome.BAR_CHART);

		/*menu.addView(new SampleCrudView(), SampleCrudView.VIEW_NAME,
        		SampleCrudView.VIEW_NAME, FontAwesome.EDIT);*/



		menu.addView(new AboutView(), AboutView.VIEW_NAME, AboutView.VIEW_NAME,
				FontAwesome.INFO_CIRCLE);

		navigator.addViewChangeListener(viewChangeListener);



		addComponent(menu);
		addComponent(viewContainer);
		setExpandRatio(viewContainer, 1);
		setSizeFull();

		if (navigator.getState().isEmpty()) {
			navigator.navigateTo(IntroPanel.VIEW_NAME);
			Page page = UIOntoKeeper.get().getPage();
			page.setUriFragment("", false);
		}


	}

	// notify the view menu about view changes so that it can display which view
	// is currently active
	ViewChangeListener viewChangeListener = new ViewChangeListener() {

		@Override
		public boolean beforeViewChange(ViewChangeEvent event) {
			return true;
		}

		@Override
		public void afterViewChange(ViewChangeEvent event) {
			menu.setActiveView(event.getViewName());
		}

	};
}
