package edu.tmc.uth.ontokeeper;

import javax.servlet.annotation.WebServlet;

import org.apache.commons.lang3.StringEscapeUtils;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.annotations.Viewport;
import com.vaadin.annotations.Widgetset;
import com.vaadin.server.Responsive;
import com.vaadin.server.SessionDestroyEvent;
import com.vaadin.server.SessionDestroyListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;
import com.vaadin.ui.themes.ValoTheme;

import edu.tmc.uth.ontokeeper.backend.DataServerService;
import edu.tmc.uth.ontokeeper.external.expert.ExpertScreen;
import edu.tmc.uth.ontokeeper.login.AccessControl;
import edu.tmc.uth.ontokeeper.login.BasicAccessControl;
import edu.tmc.uth.ontokeeper.login.LoginScreen;
import edu.tmc.uth.ontokeeper.login.LoginScreen.LoginListener;
import edu.tmc.uth.ontokeeper.main.MainScreen;

/**
 * Main UI class of the application that shows either the login screen or the
 * main view of the application depending on whether a user is signed in.
 *
 * The @Viewport annotation configures the viewport meta tags appropriately on
 * mobile devices. Instead of device based scaling (default), using responsive
 * layouts.
 */
@Viewport("user-scalable=no,initial-scale=1.0")
@Theme("ontokeeper")
@Widgetset("edu.tmc.uth.ontokeeper.OntoKeeperWidgets")
public class UIOntoKeeper extends UI {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1482543637560502779L;
	private AccessControl accessControl = new BasicAccessControl();
	private static DataServerService dds = null;

	@Override
	protected void init(VaadinRequest vaadinRequest) {
		Responsive.makeResponsive(this);
		setLocale(vaadinRequest.getLocale());
		getPage().setTitle("OntoKeeper");
		this.setApplicationsSessionVariables();
		//System.out.println("Contents of url: " + vaadinRequest.getPathInfo());

		if((vaadinRequest.getPathInfo().length() > 1) && 
				(doesExpertAccessExist(vaadinRequest.getPathInfo())
						))
		{

			setContent(new ExpertScreen(vaadinRequest.getPathInfo().substring(1)));

			
		}
		else if (!accessControl.isUserSignedIn()) {
			setContent(new LoginScreen(accessControl, new LoginListener() {
				@Override
				public void loginSuccessful() {
					showMainView();

				}
			}));
		} else {
			showMainView();
		}
	}

	private boolean doesExpertAccessExist(String urlPath){
		String pathInput = StringEscapeUtils.escapeHtml4(urlPath.substring(1));
		dds = DataServerService.get();
		
		return dds.isExpertTableAvailable(pathInput);
		


		//return true;
	}

	protected void showMainView() {
		addStyleName(ValoTheme.UI_WITH_MENU);
		setContent(new MainScreen(UIOntoKeeper.this));

		getNavigator().navigateTo(getNavigator().getState());
	}
	
	private void setApplicationsSessionVariables(){
		
		VaadinSession.getCurrent().getSession().setAttribute("SEMSService", new SEMSService()); 
		VaadinSession.getCurrent().getSession().setAttribute("ProjectProfile", new ProjectProfile());
		VaadinSession.getCurrent().getSession().setAttribute("OntologyService", new OntologyService());
		VaadinSession.getCurrent().getSession().setAttribute("WordNetService", new WordNetService());
		
	}

	public static UIOntoKeeper get() {
		return (UIOntoKeeper) UI.getCurrent();
	}

	public AccessControl getAccessControl() {
		return accessControl;
	}

	@WebServlet(urlPatterns = "/*", name = "UIOntoKeeperServlet", asyncSupported = true)
	@VaadinServletConfiguration(ui = UIOntoKeeper.class, productionMode = false)
	public static class UIOntoKeeperServlet extends VaadinServlet implements SessionDestroyListener {

		@Override
		public void sessionDestroy(SessionDestroyEvent event) {
			// TODO Auto-generated method stub
			if(dds != null){
				DataServerService dds = DataServerService.get();
			}
			
			dds.endDatabaseConnection();

		}
	}
}
