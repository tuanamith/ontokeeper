package edu.tmc.uth.ontokeeper.core;

import java.text.DecimalFormat;

import org.vaadin.risto.stepper.FloatStepper;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Slider;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

import edu.tmc.uth.ontokeeper.SEMSService;
import edu.tmc.uth.ontokeeper.data.SyntacticModel;

public class SyntacticPanel extends PanelContent {
	
	public static final String VIEW_NAME = "Syntactic";
	
	private Label lblTitleLawfullness;
	private Label lawfulnessNS;
	private Label numAxioms;
	
	private Label lblTitleRichness;
	private Label numSyntactic;
	
	private SEMSService ss = null;
	private SyntacticModel sm = null;
	
	public SyntacticPanel() {
		// TODO Auto-generated constructor stub
		
		
		createTitle(this.VIEW_NAME);
		createSubtitle("'Correctness of syntax'");
		
		createTabs();
		//super.createTitle();
	}
	
	
	
	

	private void createTabs() {
		// TODO Auto-generated method stub
		
		ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
		sm = ss.syntacitModel;
		
		TabSheet tabs = new TabSheet();
		tabs.addStyleName(ValoTheme.TABSHEET_FRAMED);
		tabs.addStyleName(ValoTheme.TABSHEET_PADDED_TABBAR);
		tabs.setSizeFull();
		
		VerticalLayout tabLawfulness = new VerticalLayout();
		
		tabLawfulness.setSpacing(true);
		tabLawfulness.setMargin(true);
		
		lblTitleLawfullness = new Label("0.00");
		//lblTitleLawfullness.setCaption("0.00");
		lblTitleLawfullness.setStyleName("h1 score-label");
		tabLawfulness.addComponent(lblTitleLawfullness);
		
		Label lawfullnessDesc = new Label("Lawfulness is adherence to syntactic rules based on "
				+ "OWL2 Profile. The lack of adherence amounts to penalties "
				+ "which are tallied and calculated against the total number of axioms. ");
		
		lawfullnessDesc.setStyleName("p");
		tabLawfulness.addComponent(lawfullnessDesc);
		
		
		lawfulnessNS = new Label("0");
		lawfulnessNS.setStyleName("strong");
		tabLawfulness.addComponent(new Label("<strong>Number of breached syntatic rules</strong>", 
				ContentMode.HTML));
		tabLawfulness.addComponent(lawfulnessNS);
		
		
		
		numAxioms = new Label("0");
		numAxioms.setStyleName("strong");
		tabLawfulness.addComponent(new Label("<strong>Number of total axioms</strong>", ContentMode.HTML));
		tabLawfulness.addComponent(numAxioms);
		
		tabs.addTab(tabLawfulness, "Lawfullness");
		
		
		///********///////
		VerticalLayout tabRichness = new VerticalLayout();
		tabRichness.setMargin(true);
		tabRichness.setSpacing(true);
		
		lblTitleRichness = new Label("0.00");
		lblTitleRichness.setStyleName("h1 score-label");
		tabRichness.addComponent(lblTitleRichness);
		
		Label lblRichnessDesc = new Label("Richness describes the amount of syntactic features from OWL2 "
				+ "that the ontology utilizes. A high richness score indicates that the "
				+ "ontology utilizes a wide variety of ontology features.");
		
		lblRichnessDesc.setStyleName("p");
		tabRichness.addComponent(lblRichnessDesc);
		
		numSyntactic = new Label("0");
		numSyntactic.setStyleName("strong");
		tabRichness.addComponent(new Label("<strong>Number of syntactical features used</strong>", 
				ContentMode.HTML));
		tabRichness.addComponent(numSyntactic);
		
		tabs.addTab(tabRichness, "Richness");
		this.addComponent(tabs);
		
		
		//create panel
		Panel weightPanel = new Panel("Adjust weights of the scores for syntactic");
		weightPanel.setSizeFull();
		
		//add content
		VerticalLayout weightLayout = new VerticalLayout();
		weightLayout.setSpacing(true);
		weightLayout.setMargin(true);
		
		//Weight Lawfullness Widget
		HorizontalLayout sLawWidget = new HorizontalLayout();
		sLawWidget.setSpacing(true);
		sLawWidget.setSizeFull();
		Slider sLaw = new Slider(0,1,2);
		sLaw.setCaption("Weight of Lawfullness");
		sLaw.setValue(sm.getWeight_lawfullness());
		sLaw.setWidth("100%");
		Label sLawLabel = new Label(Double.toString(sm.getWeight_lawfullness()));
		sLawLabel.setStyleName("h2");
		
		sLawWidget.addComponent(sLaw);
		sLawWidget.addComponent(sLawLabel);
		weightLayout.addComponent(sLawWidget);
		
		//Weight Richness Widget
		HorizontalLayout sRichWidget = new HorizontalLayout();
		sRichWidget.setSpacing(true);
		sRichWidget.setSizeFull();
		
		Slider sRich = new Slider(0,1,2);
		sRich.setCaption("Weight of Richness");
		sRich.setValue(sm.getWeight_richness());
		sRich.setWidth("100%");
		Label sRichLabel = new Label(Double.toString(sm.getWeight_richness()));
		sRichLabel.setStyleName("h2");
		
		sRichWidget.addComponent(sRich);
		sRichWidget.addComponent(sRichLabel);
		weightLayout.addComponent(sRichWidget);
		
		
		weightPanel.setContent(weightLayout);
		this.addComponent(weightPanel);
		
		//actionListeners
		
		sLaw.addValueChangeListener(ev->{
			
			//adjust the partner slider
			sRich.setValue(1-(double) ev.getProperty().getValue());
			
			
			//update object of sytnaticModel with weight
			if(sm == null){
				ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
				sm = ss.syntacitModel;
			}
			
			//System.out.println(ev.getProperty().getValue());
			sm.setWeight_lawfullness((double) ev.getProperty().getValue());
			sLawLabel.setValue(Double.toString((double) ev.getProperty().getValue()));
			
		});
		
		sRich.addValueChangeListener(ev->{
			sLaw.setValue(1-(double)ev.getProperty().getValue());
			
			//update object of sytnaticModel with weight
			if(sm == null){
				ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
				sm = ss.syntacitModel;
			}
			
			sm.setWeight_richness((double)ev.getProperty().getValue());
			sRichLabel.setValue(Double.toString((double)ev.getProperty().getValue()));
			
		});
		
		
	}





	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		DecimalFormat df = new DecimalFormat("#0.00"); 
		ss = (SEMSService)VaadinSession.getCurrent().getSession().getAttribute("SEMSService");
		sm = ss.syntacitModel;
		
		double lawfulness = sm.getLawfulness();
		
		if(lawfulness > 0.00){
			
			lblTitleLawfullness.setValue(df.format(lawfulness));
			this.numAxioms.setValue(Integer.toString(sm.axiomsCount()));
			this.lawfulnessNS.setValue(Integer.toString(sm.brokenRulesCount()));
		}
			
		
		//lblTitleLawfullness.
		
		double richness = sm.getRichness();
		if(richness > 0.00){
			lblTitleRichness.setValue(df.format(richness));
			numSyntactic.setValue(Integer.toString(sm.syntacticFeaturesUsed()));
		}
	}

}
