package edu.tmc.uth.ontokeeper;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.metrics.GCICount;
import org.semanticweb.owlapi.metrics.HiddenGCICount;
import org.semanticweb.owlapi.metrics.ReferencedClassCount;
import org.semanticweb.owlapi.metrics.ReferencedDataPropertyCount;
import org.semanticweb.owlapi.metrics.ReferencedIndividualCount;
import org.semanticweb.owlapi.metrics.ReferencedObjectPropertyCount;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAnnotationValue;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLDataPropertyExpression;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLObjectPropertyExpression;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.profiles.OWL2DLProfile;
import org.semanticweb.owlapi.profiles.OWLProfileReport;
import org.semanticweb.owlapi.profiles.OWLProfileViolation;
import org.semanticweb.owlapi.reasoner.ConsoleProgressMonitor;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import org.semanticweb.owlapi.reasoner.structural.StructuralReasonerFactory;
import org.semanticweb.owlapi.search.EntitySearcher;
import org.semanticweb.owlapi.vocab.OWLRDFVocabulary;

import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;

import edu.tmc.uth.ontokeeper.data.ontology.Lexicon;
import edu.tmc.uth.ontokeeper.data.ontology.OntologyData;

public class OntologyService {

	//private static OntologyService instance = null;
	File ontoFile = null;
	boolean isUsingUniqueIds = false;
	private OntologyData owldata = new OntologyData();

	public OntologyService() {
		// TODO Auto-generated constructor stub
	}
	
	public int brokenRules(){
		int broken_rules = 0;
		broken_rules = owldata.totalBrokenRules();
		
		return broken_rules;
	}
	
	public int duplicateTerms(){
		int duplicate_terms = 0;
		
		duplicate_terms = owldata.numDuplicateTerms();
		
		return duplicate_terms;
	}
	
	public int totalAxioms(){
		int total_axioms = 0;
		
		total_axioms = owldata.getNumAxioms();
		
		return total_axioms;
	}
	
	public int totalElements(){
		int total = 0;
		
		total = owldata.getTotalElements();
		
		return total;
	}
	
	public int totalSyntacticFeaturesUsed(){
		int total_syntax_features = 0;
		
		total_syntax_features = owldata.getNumSyntaticUsage();
		
		return total_syntax_features;
	}
	
	public int totalSyntacticFeatures(){
		int total_syntax_features = 0;
		
		total_syntax_features = owldata.getNumSyntatic();
		
		return total_syntax_features;
	}

	/*public static OntologyService getInstance(){
		if(instance == null){
			instance = new OntologyService();
		}

		return instance;
	}*/
	
	public Set<String> getOWLTerms(){
		
		//return owldata.getTerms();
		return owldata.getProcessed_terms();
		//return null;
	}

	public boolean isFileLoaded(){
		
		if(ontoFile == null){
			return false;
		}

		if(ontoFile.exists()){
			return true;
		}
		else{
			return false;
		}
	}

	public void loadOntologyFile(String fileLocation){
		ontoFile = new File(fileLocation);
	}

	public void loadOntologyFile(String fileLocation,  boolean isUsingUniqueIds){
		ontoFile = new File(fileLocation);
		this.isUsingUniqueIds = isUsingUniqueIds;
	}

	public void init() throws OWLOntologyCreationException{


		OWLOntologyManager owl_manager = OWLManager.createOWLOntologyManager();
		OWLDataFactory df = owl_manager.getOWLDataFactory();
		OWLAnnotationProperty label = df.getOWLAnnotationProperty(OWLRDFVocabulary.RDFS_LABEL.getIRI());

		OWLReasonerFactory reasonerFactory = new StructuralReasonerFactory();
		ConsoleProgressMonitor progressMonitor = new ConsoleProgressMonitor();
		OWLReasonerConfiguration config = new SimpleConfiguration(progressMonitor);


		OWLOntology targetOntology = owl_manager.loadOntologyFromOntologyDocument(ontoFile);
		System.out.println("Loaded ontology: " + targetOntology);
		OWLReasoner reasoner = reasonerFactory.createReasoner(targetOntology, config);
		System.out.println("Starting...");
		
		//get Profile violations
		System.out.println("Getting violation data");
		getProfileViolationFromOntology(targetOntology);

		//getting terms from classes
		System.out.println("Getting the terms....");
		
		/*
		getTermsFromOntology(targetOntology);
		System.out.println("Get number of terms: " + owldata.getTerms().size());

		//getting terms from instances
		System.out.println("Getteing terms from instances");
		getInstanceTermsFromOntology(targetOntology, reasoner,label);

		//getting terms from object properties
		System.out.println("Getting terms from object properties");
		getObjectPropertyTermsFromOntology(targetOntology, label);

		//getting terms from data properties
		System.out.println("Getting terms from data properties");
		getDataPropertyTermsFromOntology(targetOntology, label);

		

		System.out.println("Capturing number of elements from ontology");
		getNumberOfElementsFromOntology(targetOntology);
		*/ 
		extractLogicalAxiomTerms(targetOntology,df);
		//count number of statements
		//final Set<OWLAxiom> ALL_AXIOMS = targetOntology.getAxioms();
		//final Set<OWLLogicalAxiom> ALL_AXIOMS = targetOntology.getLogicalAxioms();
		owldata.setNumAxioms(targetOntology.getAxiomCount());
		//owldata.setNumAxioms(targetOntology.getLogicalAxiomCount());

		//preprocess the Terms
		cleanUpTerms(owldata);
		
		System.out.println("Number of elements: " + owldata.getTotalElements());

		//System.out.println("Added processed terms: " + terms.size() + " : " + terms.get(3).getProcessed_term());
		//System.out.println("Number of raw terms: " + owldata.getTerms().size());

	}

	private void getTermsFromOntology(OWLOntology targetOntology){
		
		for (OWLClass cls : targetOntology.getClassesInSignature()){
			
			if(isUsingUniqueIds==false)owldata.insertTerm(cls.getIRI().getFragment());

			

			for(OWLAnnotation annotation : EntitySearcher.getAnnotations(cls,targetOntology)){
		
				if(annotation.getValue() instanceof OWLLiteral){
					OWLLiteral val = (OWLLiteral) annotation.getValue();

					if (val.hasLang("en")) {
						owldata.insertTerm(val.getLiteral());
					}
					else{
						owldata.insertTerm(val.getLiteral());
					}



				}

			}

		}
	}
	
	private void extractLogicalAxiomTerms(OWLOntology targetOntology, OWLDataFactory df){
		
		
		
		Set<String>classList = new HashSet<String>();
		Set<String>datapropertyList = new HashSet<String>();
		Set<String>objectpropertyList = new HashSet<String>();
		Set<String>instanceList = new HashSet<String>();
		
		for(OWLLogicalAxiom oa: targetOntology.getLogicalAxioms()){
			for(OWLEntity oe : oa.getSignature()){
				if(oe.getEntityType().getName() == "Class"){
					classList.add(this.addClassLabel(oe.asOWLClass(), targetOntology, df));
					//countClass++;
				}
				else if(oe.getEntityType().getName() == "DataProperty"){
					datapropertyList.add(this.addPropertyLabel(oe.asOWLDataProperty(), targetOntology, df));
					//countDataProperty++;
				}
				else if(oe.getEntityType().getName()=="ObjectProperty"){
					objectpropertyList.add(this.addPropertyLabel(oe.asOWLObjectProperty(), targetOntology, df));
					//countObjectProperty++;
				}
				else if(oe.getEntityType().getName() == "NamedIndividual"){
					instanceList.add(this.addInstanceLabel(oe.asOWLNamedIndividual(), targetOntology, df));
					//countIndividual++;
					
				}
				else{
					//System.out.println(oe.getEntityType().getName());
				}
				
				
			}
		}

		owldata.setNumClasses(classList.size());
		owldata.setNumDataProperties(datapropertyList.size());
		owldata.setNumObjectProperties(objectpropertyList.size());
		owldata.setNumInstances(instanceList.size());
		
		for(String cterm : classList){
			owldata.insertTerm(cterm);
		}
		
		for(String dpterm : datapropertyList){
			owldata.insertTerm(dpterm);
		}
		
		for(String opterm : objectpropertyList){
			owldata.insertTerm(opterm);
		}
		
		for(String iterm : instanceList){
			owldata.insertTerm(iterm);
			
		}
		
		if(instanceList.contains("Child"))System.out.println("Yes");
		
		owldata.setNumSyntaticUsage(getNumSyntaticUsage(targetOntology));
		
	}
	private String addInstanceLabel(OWLNamedIndividual i, OWLOntology targetOntology, OWLDataFactory df){
		String label = "";
		for(OWLAnnotation a: EntitySearcher.getAnnotations(i.asOWLNamedIndividual(), targetOntology, df.getRDFSLabel())){
			OWLAnnotationValue val = a.getValue();
			if(val instanceof OWLLiteral){
				label = ((OWLLiteral)val).getLiteral();
				
				
			}
		} 
		
		if(label == ""){
			label = i.toString();
		}
		
		//System.out.println(label);
		
		return label;
	}
	private String addPropertyLabel(OWLDataPropertyExpression op, OWLOntology targetOntology, OWLDataFactory df){
		String label = "";
		for(OWLAnnotation a: EntitySearcher.getAnnotations(op.asOWLDataProperty(),targetOntology, df.getRDFSLabel())){
			OWLAnnotationValue val = a.getValue();
			if(val instanceof OWLLiteral){
				label = ((OWLLiteral)val).getLiteral();
				
				
			}
		}
		if(label == ""){
			label = op.toString();
		}
		
		return label;
	}
	
	private String addPropertyLabel(OWLObjectPropertyExpression op, OWLOntology targetOntology, OWLDataFactory df){
		String label = "";
		for(OWLAnnotation a: EntitySearcher.getAnnotations(op.asOWLObjectProperty(),targetOntology, df.getRDFSLabel())){
			OWLAnnotationValue val = a.getValue();
			if(val instanceof OWLLiteral){
				label = ((OWLLiteral)val).getLiteral();
				//owldata.insertTerm(label);
				
			}
		}
		
		if(label == ""){
			label = op.toString();
		}
		
		return label;
	}
	private String addClassLabel(OWLClassExpression oce, OWLOntology targetOntology, OWLDataFactory df){
		String label = "";
		
		for(OWLAnnotation a : EntitySearcher.getAnnotations(oce.asOWLClass(), targetOntology, df.getRDFSLabel() )){
			OWLAnnotationValue val = a.getValue();
			
			if(val instanceof OWLLiteral){
                label = ((OWLLiteral)val).getLiteral();
                //System.out.println("From within: " + label);
                //owldata.insertTerm(label);
			}
		}
		if(label == ""){
			label = oce.toString();
		}
		
		//System.out.println(oce.toString());
		
		
		return label;
		
	}
	
	

	private void getInstanceTermsFromOntology(OWLOntology targetOntology, OWLReasoner reasoner,OWLAnnotationProperty label ) {
		// TODO Auto-generated method stub
		for (OWLClass cls : targetOntology.getClassesInSignature()){
			for (OWLNamedIndividual i :reasoner.getInstances(cls, true).getFlattened()){

				//TODO deal with duplicate individuals

				if(isUsingUniqueIds==false) owldata.insertTerm(i.getIRI().getFragment());



				for(OWLAnnotation annotation: EntitySearcher.getAnnotations(i,targetOntology)){
					if(annotation.getValue() instanceof OWLLiteral){
						OWLLiteral val = (OWLLiteral)annotation.getValue();
						//System.out.println("Individuals: " + val.getLiteral());
						//terms.add(val.getLiteral().replaceAll("\\(.+?\\)", ""));  
						owldata.insertTerm(val.getLiteral());

					}
				}

			}
		}
	}

	private void getDataPropertyTermsFromOntology(OWLOntology targetOntology, OWLAnnotationProperty label) {
		// TODO Auto-generated method stub

		for(OWLDataProperty dp:targetOntology.getDataPropertiesInSignature()){

			if(isUsingUniqueIds==false)owldata.insertTerm(dp.getIRI().getFragment());

			for(OWLAnnotation annotation: EntitySearcher.getAnnotations(dp,targetOntology)){
				if(annotation.getValue() instanceof OWLLiteral){
					OWLLiteral val = (OWLLiteral)annotation.getValue();
					if (val.hasLang("en")) {
						owldata.insertTerm(val.getLiteral());
					}
				}
			}
		}
	}

	private void getObjectPropertyTermsFromOntology(OWLOntology targetOntology, OWLAnnotationProperty label) {
		// TODO Auto-generated method stub

		for(OWLObjectProperty op:targetOntology.getObjectPropertiesInSignature()){

			if(isUsingUniqueIds==false)owldata.insertTerm(op.getIRI().getFragment());

			for(OWLAnnotation annotation: EntitySearcher.getAnnotations(op,targetOntology)){
				if(annotation.getValue() instanceof OWLLiteral){
					OWLLiteral val = (OWLLiteral)annotation.getValue();
					if (val.hasLang("en")) {
						owldata.insertTerm(val.getLiteral());
					}
				}
			}
		}

	}

	private void getProfileViolationFromOntology(OWLOntology drugOntology) {
		// TODO Auto-generated method stub
		OWL2DLProfile profile = new OWL2DLProfile();
		OWLProfileReport report = profile.checkOntology(drugOntology);

		for(OWLProfileViolation v:report.getViolations()) {

			owldata.addRulesBroken(v.toString());
		}

		System.out.println("Number of violations: " + report.getViolations().size());
	}

	private void getNumberOfElementsFromOntology(OWLOntology ontology) {
		// TODO Auto-generated method stub

		System.out.println("Number of axioms: " + ontology.getAxiomCount());

		owldata.setNumClasses(ontology.getClassesInSignature().size());
		owldata.setNumObjectProperties(ontology.getObjectPropertiesInSignature().size());
		owldata.setNumDataProperties(ontology.getDataPropertiesInSignature().size());
		owldata.setNumInstances(ontology.getIndividualsInSignature().size());

		owldata.setNumSyntaticUsage(getNumSyntaticUsage(ontology));
		
		System.out.println("Number of elements: " + owldata.getTotalElements());
	}

	private static int getNumSyntaticUsage(OWLOntology ontology){
		int count = 0;

		//get class syntax usage
		if(new ReferencedClassCount(ontology).getValue().intValue()>0)count++;

		//get object property usage
		if(new ReferencedObjectPropertyCount(ontology).getValue().intValue()>0)count++;

		//get data property usage
		if(new ReferencedDataPropertyCount(ontology).getValue().intValue()>0)count++;

		//get individual syntax
		if( new ReferencedIndividualCount(ontology).getValue().intValue()>0)count++;

		//get GCI
		if(new GCICount(ontology).getValue().intValue()>0)count++;

		//get hidden gci
		if(new HiddenGCICount(ontology).getValue().intValue()>0)count++;

		//get subclass syntax
		if(ontology.getAxiomCount(AxiomType.SUBCLASS_OF)>0)count++;

		//get equivalentclass syntax
		if(ontology.getAxiomCount(AxiomType.EQUIVALENT_CLASSES)>0)count++;

		//get disjoint syntax
		if(ontology.getAxiomCount(AxiomType.DISJOINT_CLASSES)>0)count++;

		// subobjectproperty syntax
		if(ontology.getAxiomCount(AxiomType.SUB_OBJECT_PROPERTY)>0)count++;

		//equavlient object property syntax
		if(ontology.getAxiomCount(AxiomType.EQUIVALENT_OBJECT_PROPERTIES)>0)count++;

		//inverse object preoperty sytnax
		if(ontology.getAxiomCount(AxiomType.INVERSE_OBJECT_PROPERTIES)>0)count++;

		//disjoint object property syntax
		if(ontology.getAxiomCount(AxiomType.DISJOINT_OBJECT_PROPERTIES)>0)count++;

		//functional object property syntax
		if(ontology.getAxiomCount(AxiomType.FUNCTIONAL_OBJECT_PROPERTY)>0)count++;

		//inverse functional object syntax
		if(ontology.getAxiomCount(AxiomType.INVERSE_FUNCTIONAL_OBJECT_PROPERTY)>0)count++;

		//transitive object syntax
		if(ontology.getAxiomCount(AxiomType.TRANSITIVE_OBJECT_PROPERTY)>0)count++;

		//symmetric object syntax
		if(ontology.getAxiomCount(AxiomType.SYMMETRIC_OBJECT_PROPERTY)>0)count++;

		//assymettric object syntax
		if(ontology.getAxiomCount(AxiomType.ASYMMETRIC_OBJECT_PROPERTY)>0)count++;

		//reflexisive object property 
		if(ontology.getAxiomCount(AxiomType.REFLEXIVE_OBJECT_PROPERTY)>0)count++;

		//irrflexisive object property
		if(ontology.getAxiomCount(AxiomType.IRREFLEXIVE_OBJECT_PROPERTY)>0)count++;

		//object property domain
		if(ontology.getAxiomCount(AxiomType.OBJECT_PROPERTY_DOMAIN)>0)count++;

		//object property range
		if(ontology.getAxiomCount(AxiomType.OBJECT_PROPERTY_RANGE)>0)count++;

		//subproperty chain of
		if(ontology.getAxiomCount(AxiomType.SUB_PROPERTY_CHAIN_OF)>0)count++;

		//subdataproperty
		if(ontology.getAxiomCount(AxiomType.SUB_DATA_PROPERTY)>0)count++;

		//equivalentdataproperty
		if(ontology.getAxiomCount(AxiomType.EQUIVALENT_DATA_PROPERTIES)>0)count++;

		//disjointdataproperty
		if(ontology.getAxiomCount(AxiomType.DISJOINT_DATA_PROPERTIES)>0)count++;

		//functional data property
		if(ontology.getAxiomCount(AxiomType.FUNCTIONAL_DATA_PROPERTY)>0)count++;

		//data property domain
		if(ontology.getAxiomCount(AxiomType.DATA_PROPERTY_DOMAIN)>0)count++;

		//data property range
		if(ontology.getAxiomCount(AxiomType.DATA_PROPERTY_RANGE)>0)count++;

		//class assertion
		if(ontology.getAxiomCount(AxiomType.CLASS_ASSERTION)>0)count++;

		//object property assertion
		if(ontology.getAxiomCount(AxiomType.OBJECT_PROPERTY_ASSERTION)>0)count++;

		//data property assertion
		if(ontology.getAxiomCount(AxiomType.DATA_PROPERTY_ASSERTION)>0)count++;

		//negative data property assertion
		if(ontology.getAxiomCount(AxiomType.NEGATIVE_DATA_PROPERTY_ASSERTION)>0)count++;

		if(ontology.getAxiomCount(AxiomType.NEGATIVE_OBJECT_PROPERTY_ASSERTION)>0)count++;

		//same individual
		if(ontology.getAxiomCount(AxiomType.SAME_INDIVIDUAL)>0)count++;

		//different individual
		if(ontology.getAxiomCount(AxiomType.DIFFERENT_INDIVIDUALS)>0)count++;

		//annotation assertion
		if(ontology.getAxiomCount(AxiomType.ANNOTATION_ASSERTION)>0)count++;

		//annotation property domain
		if(ontology.getAxiomCount(AxiomType.ANNOTATION_PROPERTY_DOMAIN)>0)count++;

		//annotation preoprty range
		if(ontology.getAxiomCount(AxiomType.ANNOTATION_PROPERTY_RANGE)>0)count++;



		//count = count + ontology.getAxiomCount(AxiomType.DECLARATION);
		System.out.println("Number: " + count);

		return count;

	}
	
	private void cleanUpTerms(OntologyData owldata2) {
		// TODO Auto-generated method stub
		
		ProjectProfile pp = (ProjectProfile)VaadinSession.getCurrent().getSession().getAttribute("ProjectProfile");
		WordNetService wn = (WordNetService)VaadinSession.getCurrent().getSession().getAttribute("WordNetService");
		//pp.printSelection();
	
		
		//terms = new ArrayList<Lexicon>();
		ArrayList<Lexicon>terms = wn.getTerms();
		for(String term: owldata2.getTerms()){
			String processed_term = term;
			if(processed_term !=null || !processed_term.isEmpty()){

				if(pp.isCamelCase()){
					processed_term = buildStringFromArray(StringUtils.splitByCharacterTypeCamelCase(processed_term));
				}

				if(pp.isDetermiters()){
					String pattern = " a | the | an";
					processed_term = processed_term.replaceAll(pattern, "");
				}

				if(pp.isBrackets()){

					processed_term = processed_term.replaceAll("(\\(.+?\\))|(\\{.+?\\})", "");

				}
				if(pp.isUnderscore()){

					processed_term = processed_term.replaceAll("_", " ");

				}
				if(pp.isDashes()){
					processed_term = processed_term.replaceAll("-", " ");
				}

				owldata2.insertProcessedTerm(processed_term);
				//terms.add(new Lexicon().setProcessed_term(processed_term));
				Lexicon item = new Lexicon();
				item.setProcessed_term(processed_term); 
				item.setTerm(term);
				terms.add(item);
				

			}
		}

	}
	
	private String buildStringFromArray(String [] arrayString){
		StringBuilder builder = new StringBuilder();

		for(String s : arrayString) {
			builder.append(" " + s);
		}

		return builder.toString();
	}

	/*private void getTermsFromOntology(OWLOntology targetOntology){
		for (OWLClass cls : targetOntology.getClassesInSignature()){

			if(isUsingID==false)owldata.insertTerm(cls.getIRI().getFragment());

			for(OWLAnnotation annotation : EntitySearcher.getAnnotations(cls,targetOntology)){

				if(annotation.getValue() instanceof OWLLiteral){
					OWLLiteral val = (OWLLiteral) annotation.getValue();

					if (val.hasLang("en")) {
						owldata.insertTerm(val.getLiteral());
					}
					else{
						owldata.insertTerm(val.getLiteral());
					}



				}

			}

		}
	}*/

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
