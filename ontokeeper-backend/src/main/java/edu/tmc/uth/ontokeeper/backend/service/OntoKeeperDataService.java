package edu.tmc.uth.ontokeeper.backend.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import edu.tmc.uth.ontokeeper.backend.DataServerService;
import edu.tmc.uth.ontokeeper.backend.data.NL;
import edu.tmc.uth.ontokeeper.backend.data.Ontology;
import edu.tmc.uth.ontokeeper.backend.data.Volunteers;

public class OntoKeeperDataService extends DataServerService {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/*
	 * add CRUD functions
	 * 
	 */
	private static OntoKeeperDataService INSTANCE;
	Connection conn = null;
	Statement statement = null;

	private ArrayList<NL> naturallanguage = new ArrayList<NL>();
	private ArrayList<Volunteers> v = new ArrayList<Volunteers>();
	private ArrayList<Ontology> ontologies = new ArrayList<Ontology>();

	//private long ontology_key = 0;
	//private long user_id = 0;

	public OntoKeeperDataService() {
		// TODO Auto-generated constructor stub
		try {
			Class.forName("org.postgresql.Driver");

			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/OntokeeperDB", "ontokeeper", "cprit");
			conn.setAutoCommit(false);

			statement = conn.createStatement();

		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public synchronized static DataServerService getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new OntoKeeperDataService();
		}
		return INSTANCE;
	}

	public static void main(String[] args) {
		OntoKeeperDataService ok = new OntoKeeperDataService();
	}

	

	@Override
	public List getNLData(long key) {
		// TODO Auto-generated method stub
		naturallanguage.clear();
		
		try {
			ResultSet result = statement.executeQuery("SELECT * FROM nldata WHERE onto_id=" + key);
			while (result.next()) {
				NL nlObject = new NL();
				nlObject.setId(result.getInt("id"));
				nlObject.setIs_correct(result.getBoolean("is_correct"));
				nlObject.setNl(result.getString("nl"));
				nlObject.setNl_mod(result.getString("nl_mod"));
				nlObject.setOnto_id(result.getInt("onto_id"));
				nlObject.setNotes(result.getString("notes"));

				naturallanguage.add(nlObject);

			}

			// System.out.println("Size is " + naturallanguage.size());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return naturallanguage;
	}

	@Override
	public List averageEvaluatedStatements(long onto_id) {
		// TODO Auto-generated method stub
		List<Double> a_scores = new ArrayList<Double>();

		Set<String> urls = new HashSet<String>();
		try {
			ResultSet result = statement.executeQuery("SELECT url FROM volunteers Where onto_id=" + onto_id);
			while (result.next()) {
				urls.add(result.getString("url"));
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		System.out.println("Got number of table-urls: " + urls.size());
		boolean gotSize = false;
		long sizeOfTable = 0;
		for (String url : urls) {

			try {
				ResultSet resultSize = statement.executeQuery("SELECT COUNT(*) as size FROM " + url);
				while (resultSize.next()) {
					System.out.println("Total size: " + resultSize.getLong("size"));
					sizeOfTable = resultSize.getLong("size");
				}

				long numTrue = 0;
				ResultSet result2 = statement.executeQuery("SELECT * FROM " + url + " WHERE is_correct='TRUE'");
				while (result2.next()) {
					numTrue++;
					// System.out.println("Average output: " +
					// result2.getInt("id"));
				}

				double accuracy_score = ((double) numTrue / sizeOfTable);
				System.out.println("Accuary : " + new DecimalFormat("#.##").format(accuracy_score));
				a_scores.add(accuracy_score);

			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return a_scores;
	}

	@Override
	public List getVolunteers(long onto_id) {
		// TODO Auto-generated method stub
		v.clear();
		try {
			ResultSet result = statement.executeQuery("SELECT * FROM volunteers Where onto_id=" + onto_id);
			while (result.next()) {
				Volunteers vObject = new Volunteers();
				vObject.setEmail(result.getString("email"));
				vObject.setName(result.getString("name"));
				vObject.setUrl(result.getString("url"));
				vObject.setId(result.getInt("id"));

				v.add(vObject);

			}

			// System.out.println("Size of volunteer table is " + v.size());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return v;
	}

	@Override
	public void deleteVolunteer(int id) {
		// TODO Auto-generated method stub
		String sqlDelete = "DELETE from volunteers where id=" + id;

		System.out.println(sqlDelete);
		try {
			// statement = conn.createStatement();
			statement.executeUpdate(sqlDelete);
			conn.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void addVolunteerTable(long volunteerId, String tableID, long onto_id) {

		String createTable = "CREATE TABLE " + tableID + " AS (SELECT * FROM nldata WHERE onto_id=" + onto_id
				+ ")";

		// System.out.println(createTable);

		try {
			statement.executeUpdate(createTable);
			conn.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void addVolunteer(Volunteers volunteer, long onto_id) {
		// TODO Auto-generated method stub

		if (onto_id == 0) {

			return;
		}

		String sql = "INSERT INTO volunteers (name,email,url,onto_id) " + "VALUES ('" + volunteer.getName() + "', '"
				+ volunteer.getEmail() + "', '" + volunteer.getUrl() + "'," + onto_id + ") RETURNING id";

		try {
			ResultSet rs = statement.executeQuery(sql);
			conn.commit();
			rs.next();
			// System.out.println("New key: " + rs.getLong(1));

			this.addVolunteerTable(rs.getLong(1), volunteer.getUrl(), onto_id);

			// statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

	}

	@Override
	public void saveNLData(NL nl_statements) {
		// TODO Auto-generated method stub

		String sql = "INSERT INTO nl ()";

	}

	@Override
	public void updateNLData(NL data, String table) {
		// TODO Auto-generated method stub
		if (table == null) {
			table = "nldata";
		}

		String sql = "Update " + table + " SET nl='" + data.getNl() + "', is_correct=" + data.isIs_correct()
				+ ", notes='" + data.getNotes() + "' WHERE id=" + data.getId();

		// System.out.println(sql);

		try {
			statement.executeUpdate(sql);
			conn.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public long addOntologyRegistryEntry(String userId, String filePath) {
		// TODO Auto-generated method stub
		long result = 0;
		String sqlString = "INSERT INTO ontology (user_id, filepath) " + "VALUES (" + Long.parseLong(userId) + ", '"
				+ filePath + "') RETURNING id";

		try {

			ResultSet rs = statement.executeQuery(sqlString);
			conn.commit();
			rs.next();
			result = rs.getLong(1);
			// System.out.println("the id is " + rs.getLong(1));

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

		return result;

	}

	@Override
	public boolean addGeneratedStatements(long ontology_key, ArrayList<String> statements) {

		//this.ontology_key = ontology_key;
		try {

			String sqlString = "INSERT into nldata (onto_id, nl) " + "VALUES (?,?)";

			PreparedStatement ps = conn.prepareStatement(sqlString);
			for (String nl_statement : statements) {

				// System.out.println(nl_statement);
				/*
				 * String sqlString =
				 * "INSERT into nl_master (onto_id, statement) " + "VALUES (" +
				 * ontology_key + ", " + nl_statement +"')";
				 */
				// System.out.println(nl_statement);
				ps.setLong(1, ontology_key);
				ps.setString(2, nl_statement);
				// statement.addBatch(sqlString);
				ps.addBatch();
				// conn.commit();

			}
			ps.executeBatch();
			conn.commit();
			// statement.executeBatch();
			// conn.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public long saveOntologyFile(File ontoFile, String evaluationName, long user_id) {
		long ontology_key = 0;
		try {
			FileInputStream fis = new FileInputStream(ontoFile);
			PreparedStatement ps = conn.prepareStatement(
					"INSERT INTO ontology(user_id, filepath, file, filename, evaluationsession, sessiondate) VALUES (?,?,?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			ps.setString(6, new Date().toString());
			ps.setString(5, evaluationName);
			ps.setString(4, ontoFile.getName());
			ps.setString(2, ontoFile.getAbsolutePath());
			ps.setLong(1, user_id);
			ps.setBinaryStream(3, fis, (int) ontoFile.length());
			if (ps.executeUpdate() > 0) {

				conn.commit();

				ResultSet rs = ps.getGeneratedKeys();
				if (rs.next()) {
					// System.out.println("the key is " + rs.getLong(1));
					ontology_key = rs.getLong(1);
				}
				ps.close();
				fis.close();
			}

		} catch (FileNotFoundException | SQLException e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ontology_key;
	}

	/**
	 * @return the ontology_key
	 */
	public long getOntology_key() {
		//return ontology_key;
		return 0;
	}

	/**
	 * @param ontology_key
	 *            the ontology_key to set
	 */
	public void setOntology_key(long ontology_key) {
		//this.ontology_key = ontology_key;
	}

	@Override
	public File retrieveOntologyFile(long index, String toLocation) {

		//this.ontology_key = index;
		File ontoFile = null;
		FileOutputStream fos = null;
		String sqlString = "SELECT file, LENGTH(file), filename FROM ontology where id=" + index;
		try {
			// LargeObjectManager lobj =
			// ((org.postgresql.PGConnection)conn).getLargeObjectAPI();
			// System.out.println(sqlString);
			PreparedStatement pt = conn.prepareStatement(sqlString);
			ResultSet rs = pt.executeQuery();
			// conn.commit();
			while (rs.next()) {
				ontoFile = new File(toLocation + rs.getString("filename"));
				fos = new FileOutputStream(ontoFile);
				// LargeObject obj = lobj.open(rs.getLong(1),
				// LargeObjectManager.READ);
				// byte buf[] = new byte[obj.size()];
				// obj.read(buf, 0, obj.size());
				// fos.write(buf, 0, obj.size());

				byte[] filebytes = rs.getBytes(1);
				fos.write(filebytes, 0, rs.getInt(2));

				// obj.close();
			}

			rs.close();
			pt.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ontoFile;
	}

	@Override
	public List getOntologies(long user_id) {
		ontologies.clear();

		String sqlString = "SELECT id, user_id, filepath, filename, evaluationsession, sessiondate from ontology where user_id="
				+ user_id;

		try {
			ResultSet result = statement.executeQuery(sqlString);

			while (result.next()) {

				Ontology ontology = new Ontology();
				ontology.setId(result.getLong("id"));
				ontology.setUser_id(result.getLong("user_id"));
				ontology.setFilepath(result.getString("filepath"));
				ontology.setFilename(result.getString("filename"));
				ontology.setEvaluationsession(result.getString("evaluationsession"));
				ontology.setSessiondate(result.getString("sessiondate"));

				ontologies.add(ontology);

			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ontologies;
	}

	@Override
	public List getUserOntologies(long userId) {
		// TODO Auto-generated method stub
		ontologies.clear();

		return ontologies;
	}

	@Override
	public void endDatabaseConnection() {

		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	@Override
	public List getExpertNLDataByName(String tableName) {

		naturallanguage.clear();
		// System.out.println("Table name: " + tableName);
		try {
			ResultSet result = statement.executeQuery("SELECT * FROM " + tableName);
			while (result.next()) {
				NL nlObject = new NL();
				nlObject.setId(result.getInt("id"));
				nlObject.setIs_correct(result.getBoolean("is_correct"));
				nlObject.setNl(result.getString("nl"));
				nlObject.setNl_mod(result.getString("nl_mod"));
				nlObject.setOnto_id(result.getInt("onto_id"));
				nlObject.setNotes(result.getString("notes"));

				naturallanguage.add(nlObject);

			}

			System.out.println("Size is " + naturallanguage.size());

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return naturallanguage;
	}

	@Override
	public boolean isExpertTableAvailable(String pathInput) {

		boolean result = false;

		try {
			DatabaseMetaData meta = conn.getMetaData();
			ResultSet rs = meta.getTables(null, null, null, new String[] { "TABLE" });

			while (rs.next()) {

				if (rs.getString("TABLE_NAME").equals(pathInput.toLowerCase())) {
					// System.out.println("found table");
					result = true;
				}
			}
			rs.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public boolean canLogIn(String username, String password) {

		try {
			if (statement.isClosed()) {
				reOpenDatabase();
			}

		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		String sqlString = "SELECT * FROM users WHERE username='" + username + "' AND password='" + password + "'";

		System.out.println(sqlString);

		try {
			ResultSet result = statement.executeQuery(sqlString);

			while (result.next()) {
				// result.getInt("id");
				
				//user_id = result.getLong("id");
				// System.out.println("Exist");
				return true;
			}

			result.close();
			/*
			 * while(result.next()){
			 * 
			 * }
			 */
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public long getUserId(String username, String password) {
		// TODO Auto-generated method stub
		long userId = 0;
		
		String sqlString = "SELECT * FROM users WHERE username='" + username + "' AND password='" + password + "'";

		//System.out.println(sqlString);

		try {
			ResultSet result = statement.executeQuery(sqlString);

			while (result.next()) {
				userId = result.getLong("id");
				
				
			}

			result.close();
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return userId;
	}

	private void reOpenDatabase() {

		try {
			Class.forName("org.postgresql.Driver");
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/OntokeeperDB", "ontokeeper", "cprit");
			conn.setAutoCommit(false);
			System.out.println("ReOpened database successfully");

			statement = conn.createStatement();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void deleteOntology(long id) {
		// TODO Auto-generated method stub

		String deleteStr = "DELETE FROM ontology WHERE id=" + id;

		System.out.println("delete query: " + deleteStr);
		try {
			// statement = conn.createStatement();
			statement.executeUpdate(deleteStr);

			conn.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public HashMap<String, String> getUserData(long user_id) {
		// TODO Auto-generated method stub
		Map<String, String> userData = new HashMap<String, String>();

		String queryUser = "SELECT * FROM users WHERE id=" + user_id;

		System.out.println("user query: " + queryUser);

		try {
			ResultSet resultset = statement.executeQuery(queryUser);
			ResultSetMetaData metadata = resultset.getMetaData();
			int count = metadata.getColumnCount();
			while (resultset.next()) {
				for (int i = 1; i <= count; i++) {
					String label = metadata.getColumnName(i);
					userData.put(label, resultset.getString(label));
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return (HashMap<String, String>) userData;
	}

	@Override
	public void setUserData(HashMap<String, String> userData, long user_id) {
		// TODO Auto-generated method stub
		
		String sqlUpdateUser = "Update users set username='" + userData.get("username") + "', password='" + userData.get("password") + "' WHERE id=" + user_id;
		
		try {
			statement.executeUpdate(sqlUpdateUser);
			conn.commit();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public int saveSnapshotData(HashMap<String, String> snapshotData, long onto_id, long user_id) {
		// TODO Auto-generated method stub
		//snapshotData.forEach((key,value)->System.out.println(key + " - " + value));
		
		int result = 0;
		
		String insertSnapshot = "INSERT INTO scoring_data (name, semantic_original,syntactic_original,pragmatic_original,weights_semantic, weights_syntactic, weights_pragmatic,date, user_id, onto_id) VALUES ("
				+ "'" + snapshotData.get("name") + "', "
				+ Double.parseDouble(snapshotData.get("semantic_original")) + ", "
				+ Double.parseDouble(snapshotData.get("syntactic_original")) + ", "
				+ Double.parseDouble(snapshotData.get("pragmatic_original")) + ", "
				+ Double.parseDouble(snapshotData.get("weights_semantic")) + ", "
				+ Double.parseDouble(snapshotData.get("weights_syntactic"))+ ", "
				+ Double.parseDouble(snapshotData.get("weights_pragmatic"))+ ", "
				+ "'" + snapshotData.get("date") + "', "
				+ user_id + "," + onto_id +")";
		
		System.out.println(insertSnapshot);
		
		try {
			result =statement.executeUpdate(insertSnapshot);
			conn.commit();
			
			//System.out.println(i);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
		
	}

	@Override
	public HashMap<String, String> getUserSnapshotData(long user_id) {
		// TODO Auto-generated method stub
		Map <String,String>userSnaps = new HashMap<String, String>();
		
		String selectSnap = "SELECT id, name from scoring_data WHERE user_id=" + user_id;
		
		try {
			ResultSet rs = statement.executeQuery(selectSnap);
			
			while(rs.next()){
				userSnaps.put(rs.getString("id"), rs.getString("name"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return (HashMap<String, String>) userSnaps;
	}

	@Override
	public HashMap<String, String> getSnapshotDataById(long id) {
		// TODO Auto-generated method stub
		Map <String, String> snapshotData = new HashMap<String, String>();
		
		String selectSnap = "SELECT * from scoring_data WHERE id=" + id;
		
		try {
			ResultSet rs = statement.executeQuery(selectSnap);
			while(rs.next()){
				snapshotData.put("semantic_original", rs.getString("semantic_original"));
				snapshotData.put("pragmatic_original", rs.getString("pragmatic_original"));
				snapshotData.put("syntactic_original", rs.getString("syntactic_original"));
				snapshotData.put("weights_semantic", rs.getString("weights_semantic"));
				snapshotData.put("weights_pragmatic", rs.getString("weights_pragmatic"));
				snapshotData.put("weights_syntactic", rs.getString("weights_syntactic"));
				snapshotData.put("date", rs.getString("date"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
		
		return (HashMap<String, String>) snapshotData;
	}

}
