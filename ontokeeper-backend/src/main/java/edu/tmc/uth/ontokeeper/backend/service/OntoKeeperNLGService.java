package edu.tmc.uth.ontokeeper.backend.service;

import java.io.File;
import java.util.ArrayList;

import edu.tmc.uth.ontokeeper.backend.NLGService;
import edu.uthouston.sbmi.hootation.GenerateStatements;

public class OntoKeeperNLGService extends NLGService{
	
	private GenerateStatements gs;
	
	private ArrayList<String>nl_statements;
	
	private static OntoKeeperNLGService INSTANCE;

	public OntoKeeperNLGService() {
		// TODO Auto-generated constructor stub
		if(gs == null){
			gs = GenerateStatements.getInstance();
			
		}
		
		nl_statements = new ArrayList<String>();
	}
	
	public synchronized static OntoKeeperNLGService getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new OntoKeeperNLGService();
        }
        return INSTANCE;
    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	@Override
	public ArrayList<String> getNLStatements() {
		// TODO Auto-generated method stub
		nl_statements = gs.getNl_statements();
		
		return nl_statements;
	}

	@Override
	public void setOntology(String filePath) {
		// TODO Auto-generated method stub
		File ontoFile = new File(filePath);
		gs.init(ontoFile);
		gs.convertAxiomsToStatements();
		
	}

	@Override
	public void clearStatements() {
		// TODO Auto-generated method stub
		gs.reset();
	}

	@Override
	public int statementCount() {
		// TODO Auto-generated method stub
		nl_statements = gs.getNl_statements();
		return nl_statements.size();
	}

}
