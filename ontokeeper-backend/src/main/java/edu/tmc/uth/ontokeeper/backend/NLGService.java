package edu.tmc.uth.ontokeeper.backend;

import java.io.Serializable;
import java.util.ArrayList;

import edu.tmc.uth.ontokeeper.backend.service.OntoKeeperNLGService;

public abstract class NLGService implements Serializable {

	/*public NLGService() {
		// TODO Auto-generated constructor stub
	}*/
	
	public abstract ArrayList<String> getNLStatements();
	
	public abstract void setOntology(String filePath);
	
	public abstract void clearStatements();
	
	public abstract int statementCount();
	
	public static NLGService get(){
		return OntoKeeperNLGService.getInstance();
	}

}
