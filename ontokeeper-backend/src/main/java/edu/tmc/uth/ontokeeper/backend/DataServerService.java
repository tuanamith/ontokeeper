package edu.tmc.uth.ontokeeper.backend;

import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import edu.tmc.uth.ontokeeper.backend.data.NL;
import edu.tmc.uth.ontokeeper.backend.data.Volunteers;
import edu.tmc.uth.ontokeeper.backend.service.OntoKeeperDataService;

public abstract class DataServerService implements Serializable{

	//public abstract long addOntologyRegistryEntry(String userId, String filePath);
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8624709250061740023L;
	
	public abstract boolean canLogIn(String username, String password);
	
	public abstract long getUserId(String username, String password);

	public abstract boolean isExpertTableAvailable(String pathInput);
	
	public abstract boolean addGeneratedStatements(long ontology_key, ArrayList<String> statements);
	
	//public abstract void updateStatement(String text);
	
	public abstract long saveOntologyFile(File ontoFile, String evaluationName, long user_id);
	
	public abstract File retrieveOntologyFile(long index, String toLocation);
	
	public abstract List getOntologies(long user_id);
	
	public abstract List getUserOntologies(long user_id);
	
	public abstract List getNLData(long ontologykey);
	
	public abstract List getExpertNLDataByName(String tableName);
	
	public abstract void saveNLData(NL nl_statements);
	
	public abstract void updateNLData(NL data, String table);
	
	public abstract List getVolunteers(long onto_id);
	
	public abstract List averageEvaluatedStatements(long onto_id);
	
	public abstract void deleteVolunteer(int id);
	
	public abstract void deleteOntology(long id);
	
	public abstract void addVolunteer(Volunteers volunteer, long onto_id);
	
	public abstract void addVolunteerTable(long volunteer_id, String tableName, long onto_id);
	
	public abstract void endDatabaseConnection();
	
	public abstract HashMap<String,String> getUserData(long user_id);
	
	public abstract void setUserData (HashMap<String,String> userData, long user_id);
	
	public abstract int saveSnapshotData(HashMap<String, String>snapshotData, long onto_id, long user_id);
	
	public abstract HashMap<String, String>getUserSnapshotData(long user_id);
	
	public abstract HashMap<String, String>getSnapshotDataById(long id);
	
	public static DataServerService get() {
        return OntoKeeperDataService.getInstance();
    }

}
