package edu.tmc.uth.ontokeeper.backend.data;

import java.util.Date;

import org.hashids.Hashids;

public class Volunteers {
	
	private int id;
	private String name;
	private String email;
	private String url;
	private String salt = "teamorg";
	/*private String notifyButton = "notify";
	private String removeButton = "delete";*/

	public Volunteers(String name, String email) {
		// TODO Auto-generated constructor stub
		this.name = name;
		this.email = email;
		
		Hashids hashids = new Hashids(this.email+this.name+salt);
		this.url = hashids.encode(new Date().getTime());
		this.alterURL();
	}
	
	private void alterURL(){
		if (this.url != null){
			if(Character.isDigit(this.url.charAt(0))){
				this.url = "_"+this.url;
			}
		}
	}
	
	public Volunteers(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
		
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		
		
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

}
