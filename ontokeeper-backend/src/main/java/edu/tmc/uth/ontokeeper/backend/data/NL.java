package edu.tmc.uth.ontokeeper.backend.data;

import java.io.Serializable;

public class NL implements Serializable{
	
	private int onto_id;
	private String nl;
	private String nl_mod;
	private String notes;
	private boolean is_correct;
	private long id;

	public NL() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the onto_id
	 */
	public int getOnto_id() {
		return onto_id;
	}

	/**
	 * @param onto_id the onto_id to set
	 */
	public void setOnto_id(int onto_id) {
		this.onto_id = onto_id;
	}

	/**
	 * @return the nl
	 */
	public String getNl() {
		return nl;
	}

	/**
	 * @param nl the nl to set
	 */
	public void setNl(String nl) {
		this.nl = nl;
	}

	/**
	 * @return the nl_mod
	 */
	public String getNl_mod() {
		return nl_mod;
	}

	/**
	 * @param nl_mod the nl_mod to set
	 */
	public void setNl_mod(String nl_mod) {
		this.nl_mod = nl_mod;
	}

	/**
	 * @return the notes
	 */
	public String getNotes() {
		return notes;
	}

	/**
	 * @param notes the notes to set
	 */
	public void setNotes(String notes) {
		this.notes = notes;
	}

	/**
	 * @return the is_correct
	 */
	public boolean isIs_correct() {
		return is_correct;
	}

	/**
	 * @param is_correct the is_correct to set
	 */
	public void setIs_correct(boolean is_correct) {
		this.is_correct = is_correct;
	}

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

}
