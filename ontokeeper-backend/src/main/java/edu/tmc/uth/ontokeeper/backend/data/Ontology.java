package edu.tmc.uth.ontokeeper.backend.data;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Ontology implements Serializable {
	
	private long id;
	private long user_id;
	private String filepath;
	private String filename;
	private String evaluationsession;
	private String sessiondate; //need to be converted to date format
	
	
	private DateFormat output = new SimpleDateFormat("MM/dd/YYYY h:mm a");
	private DateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getUser_id() {
		return user_id;
	}

	public void setUser_id(long user_id) {
		this.user_id = user_id;
	}

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getEvaluationsession() {
		return evaluationsession;
	}

	public void setEvaluationsession(String evaluationsession) {
		this.evaluationsession = evaluationsession;
	}

	

	public Ontology() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public String getSessiondate() {
		String strDate = "";
		try {
			Date session_date = df.parse(this.sessiondate);
			strDate = output.format(session_date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return strDate;
	}

	public void setSessiondate(String sessiondate) {
		this.sessiondate = sessiondate;
	}

}
